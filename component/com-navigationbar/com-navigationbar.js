import wx from '../../subwe/bridge';
import WXComponent from "../../subwe/component";
// components/com-navigationbar/com-navigationbar.js
WXComponent({
  /**
   * 组件的属性列表
   */
  properties: {
    custom: {
      type: Boolean,
      value: false
    },
    showBack: {
      type: Boolean,
      value: true
    },
    navigationBarTitleText: {
      type: String,
      value: '小电速充'
    },
    navigationBarBackground: {
      type: String,
      value: ''
    },
    navigationBarBackColor: {
      type: String,
      value: 'black'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    statusBarHeight: 20,
    menuButtonHeigth: 40,
    navigationbarHeight: 60
  },
  ready() {
    let systemInfo = wx.getSystemInfoSync();
    let menuButtonInfo = wx.getMenuButtonBoundingClientRect();
    let menuButtonTop = menuButtonInfo.top - systemInfo.statusBarHeight;
    this.setData({
      statusBarHeight: systemInfo.statusBarHeight,
      menuButtonHeigth: menuButtonInfo.height + menuButtonTop * 2
    });
    const query = wx.createSelectorQuery().in(this);
    query.select('#navigationbar').boundingClientRect(data => {
      this.setData({
        navigationbarHeight: data.height
      });
    }).exec();
  },
  /**
   * 组件的方法列表
   */
  methods: {
    toBack() {
      wx.navigateBack({
        delta: 0
      });
    }
  }
});