import WXComponent from "../../subwe/component";
const app = getApp();
WXComponent({
  properties: {
    descText: {
      type: String,
      value: "暂无当前订单信息"
    },
    emptyImage: {
      type: String,
      value: `${app.globalData.imageUrl}/zhongman2/icon_no_order@2x.png`
    },
    isCustom: {
      type: Boolean,
      value: false
    }
  },
  data: {}
});