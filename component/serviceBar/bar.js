import wx from '../../subwe/bridge';
import WXComponent from "../../subwe/component";
const app = getApp();
WXComponent({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true,
    multipleSlots: true
  },
  /**
   * 组件的对外属性
   */
  properties: {
    typePage: {
      type: String,
      default: ''
    },
    prodType: {
      type: Number,
      default: 1
    },
    bgColor: {
      type: String,
      default: ''
    },
    isCustom: {
      type: [Boolean, String],
      default: false
    },
    isBack: {
      type: [Boolean, String],
      default: false
    },
    isHome: {
      type: [Boolean, String],
      default: false
    },
    bgImage: {
      type: String,
      default: ''
    },
    serviceHomePath: {
      type: String,
      value: '/pages/serviceHome/serviceHome'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    imageUrl: `${app.globalData.imageUrl}/zhongman2`
  },
  /**
   * 组件的方法列表
   */
  methods: {
    BackPage() {
      wx.navigateBack({
        delta: 1
      });
    },
    toBooking() {
      // wx.reportEvent("feizhu_banner", {});
      wx.navigateToMiniProgram({
        appId: 'wx6a96c49f29850eb5',
        path: 'pages/hotel-search/searchlist/index?fpid=17016&ttid=12wechat000004650'
      });
    },
    toServiceIndex() {
      // wx.navigateTo({
      //     url: '/pages/padline/padline',
      // })
      wx.navigateToMiniProgram({
        appId: 'wx0e6ed4f51db9d078',
        path: 'pages/hotel/inquire/index.html'
      });
    },
    toServiceHome() {
      wx.redirectTo({
        url: this.properties.serviceHomePath
      });
    },
    toServiceCommunity() {
      wx.redirectTo({
        url: '/pages/serviceCommunity/serviceCommunity'
      });
    },
    toServiceMy() {
      wx.redirectTo({
        url: `/pages/serviceMy/serviceMy?serviceHome=${this.properties.serviceHomePath}`
      });
    }
  }
});
