import wx from '../../subwe/bridge';
import WXComponent from "../../subwe/component";
const app = getApp();
WXComponent({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true,
    multipleSlots: true
  },
  /**
   * 组件的对外属性
   */
  props: {
    navTitle: {
      type: String,
      default: ''
    },
    bgColor: {
      type: String,
      default: ''
    },
    isCustom: {
      type: [Boolean, String],
      default: false
    },
    isBack: {
      type: [Boolean, String],
      default: false
    },
    isHome: {
      type: [Boolean, String],
      default: false
    },
    bgImage: {
      type: String,
      default: ''
    },
    colorFont: {
      type: String,
      value: "#FFFFFF"
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    // 头部高度
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom
  },
  /**
   * 组件的方法列表
   */
  created() {
    const res = wx.getSystemInfoSync();
    var statusbarH = res.statusBarHeight;
    console.log('created', this.props)

    if(this.props.navTitle && !(this.props.navTitle instanceof Object)) my.setNavigationBar({ title: this.props.navTitle })
  },
  methods: {
    BackPage() {
      let pages = getCurrentPages();
      if (pages.length === 2) {
        wx.navigateBack({
          delta: 1
        });
      } else if (pages.length === 1) {
        wx.reLaunch({
          url: '/pages/serviceHome/serviceHome'
        });
      } else {
        wx.navigateBack({
          delta: 1
        });
      }
      // wx.navigateBack({
      //   delta: 2
      // });
    },

    toHome() {
      wx.reLaunch({
        // url: '/pages/padline/padline',
        url: '/pages/serviceHome/serviceHome'
      });
    }
  }
});
