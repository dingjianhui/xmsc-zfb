const isDev = true; // true:测试环境 false:正式环境
const databaseDev = false; // 数据库连接 -> true:测试库 - false:正式库

// 测试环境
const configDev = {
  baseUrl: databaseDev ? 'https://api.hnxmkj.net/zfb/v1/wx/' : "https://api.hnxmkj.net/zfb/v1/wx/"
};
// 正式环境
const configPro = {
  baseUrl: "https://api.hnxmkj.net/zfb/v1/wx/",
  yundianUrl: "https://api.hnxmkj.net/zfb/v1/wx/"
};
const config = isDev ? configDev : configPro;
module.exports = config;