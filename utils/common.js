import wx from '../subwe/bridge';
const app = getApp();
const {
  getQueryString
} = require("util");
import { addScanLog } from "../apis/api";

// function onScan() {
//     wx.scanCode({
//         success: (res) => {
//             var result = res.result;
//             let q = decodeURIComponent(result)
//             let deviceSn = getQueryString(q, "dev")
//             app.globalData.deviceSn = deviceSn
//             console.log('app.globalData.deviceSn', res.result, app.globalData.deviceSn)
//             wx.navigateTo({ url: `/pages/padline/padline?deviceSn=${deviceSn}` })
//         }
//     })
// }
// 扫码日志
export const getPageInfo = (url, params) => {
  console.log("参数===》", params);
  wx.request({
    url: url + addScanLog,
    data: params,
    method: 'post',
    header: {
      'Content-Type': 'application/json'
    },
    success(res) {},
    fail(err) {}
  });
};
module.exports = {
  getPageInfo
};