import wx from '../subwe/bridge'
import CONST from "./const"
import {
    openBluetoothGroupPasswordXm,
    openBluetoothGroupPassword,
    openBluetoothChangePassword,
    openBluetoothPassword4,
    openBluetoothPassword,
    searchBluetooth
} from './bluetoothAlipay'

const app = getApp()

// type 1-充电 2-断电 3-打开通道1 4-打开通道2
// options {bluetoothMac, powerMinute, type, success, fail}
function openBluetoothAdapterPeripheral(options) {
    if (options.powerMinute <= 1) options.powerMinute = 1
    if (!options.type) options.type = options.powerMinute <= 1 ? CONST.BluetoothTypeOffCharge : CONST.BluetoothTypeStartCharge
    if (!options.deviceSn) options.deviceSn = app.globalData.deviceSn
    options.deviceSn = wx.getStorageSync("bluetoothName") || options.deviceSn
    if (Number(wx.getStorageSync("bluetoothType")) === 2) return connectBluetoothRecharge(options)
    if (Number(wx.getStorageSync("bluetoothType")) === 3) return connectBluetoothPasswordRecharge(options)
    if (Number(wx.getStorageSync("bluetoothType")) === 4) return connectBluetooth4(options)
    if (Number(wx.getStorageSync("bluetoothType")) === 5) return openBluetoothGroupPasswordXm(options)
    else connectBluetooth(options)
}

// 主机模式 搜索蓝牙
function openBluetoothAdapter(options) {
    options.deviceSn = wx.getStorageSync("bluetoothName") || options.deviceSn
    searchBluetooth(options)
}

// 蓝牙 第四版
function connectBluetooth4(options) {
    openBluetoothPassword4(options)
}

// 蓝牙 天盘设备
function connectBluetooth(options) {
    openBluetoothPassword(options)
}

// 蓝牙 动态密码方式
function connectBluetoothRecharge(options) {
    openBluetoothChangePassword(options)
}

// 蓝牙 密码组方式
function connectBluetoothPasswordRecharge(options) {
    openBluetoothGroupPassword(options)
}

module.exports = {
    openBluetoothAdapterPeripheral,
    openBluetoothAdapter
};
