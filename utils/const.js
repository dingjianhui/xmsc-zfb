const BluetoothNamePrefixList = ['BTXC']; // 蓝牙名称前缀

const BluetoothStatusSearch = 1; // 蓝牙搜索中
const BluetoothStatusFail = 2; // 蓝牙开启失败
const BluetoothStatusSuccess = 3; // 蓝牙开启成功
const BluetoothStatusInform = 4; // 蓝牙通知成功

const BluetoothTypeStartCharge = 1; // 蓝牙开启充电
const BluetoothTypeOffCharge = 2; // 蓝牙关闭充电
const BluetoothTypeStartChannelOne = 3; // 蓝牙打开通道口1
const BluetoothTypeStartChannelTwo = 4; // 蓝牙打开通道口2

const BluetoothOrderStatusNotPayment = 1; // 订单未支付
const BluetoothOrderStatusPayment = 2; // 订单已经支付

module.exports = {
  BluetoothNamePrefixList,
  BluetoothStatusSearch,
  BluetoothStatusFail,
  BluetoothStatusSuccess,
  BluetoothStatusInform,
  BluetoothTypeStartCharge,
  BluetoothTypeOffCharge,
  BluetoothTypeStartChannelOne,
  BluetoothTypeStartChannelTwo,
  BluetoothOrderStatusNotPayment,
  BluetoothOrderStatusPayment
};