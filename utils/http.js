import wx from '../subwe/bridge';
const api = require('../apis/api');
export class Http {
  constructor() {}
  request({
    url,
    data = {},
    method,
    header,
    callback = ''
  } = {}) {
    // console.log('打印测试，请求登录信息', wx.getStorageSync('login_key'))
    let _this = this;
    return new Promise((resolve, reject) => {
      wx.request({
        url,
        data,
        method,
        header: {
          'Access-Token': 'dingo ' + wx.getStorageSync('login_key').token
        },
        callback,
        fail(res) {
          console.log("fail", res);
          reject(res);
        },
        complete: res => {
          console.log("res===>", res);
          if (callback) callback(res.data);
          let statusCode = res.status;
          if (statusCode == 404) {
            console.log('接口不存在');
          } else if (statusCode == 401) {
            reject("登录已过期");
          } else if (statusCode == 200) {
            resolve(res.data);
          } else if (statusCode.toString().startsWith('5')) {
            wx.showModal({
              title: '提示',
              content: "服务器报错，请重试！",
              showCancel: false
            });
          }
        }
      });
    });
  }
}
