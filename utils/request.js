import wx from '../subwe/bridge';
/**
 * GET请求封装
 */
function get(url, data = {}) {
  return request(url, data, 'GET');
}

/**
 * POST请求封装
 */
function post(url, data = {}) {
  return request(url, data, 'POST');
}

/**
 * 微信的request
 */
function request(url, data = {}, method = "GET") {
  var loginInfo = wx.getStorageSync("login_key");
  var contentType = 'application/json';
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': contentType,
        'Access-Token': 'dingo ' + loginInfo.token
      },
      success: function (res) {
        // console.log('===============================================================================================')
        // console.log('==    接口地址：' + url)
        // console.log('==    接口参数：' + JSON.stringify(data))
        // console.log('==    请求类型：' + method)
        // console.log("==    接口状态：" + res.statusCode);
        // console.log('===============================================================================================')
        // console.log(res)
        if (res.statusCode == 200) {
          resolve(res.data);
        } else if (res.statusCode == 401) {
          //此处验证了token的登录失效，如果不需要，可以去掉。
          //未登录，跳转登录界面
          reject("登录已过期");
        } else {
          //请求失败
          console.log(res.statusCode);
          reject("请求失败：" + res.statusCode);
        }
      },
      fail: function (err) {
        //服务器连接异常
        // console.log('===============================================================================================')
        // console.log('==    接口地址：' + url)
        // console.log('==    接口参数：' + JSON.stringify(data))
        // console.log('==    请求类型：' + method)
        // console.log("==    服务器连接异常")
        // console.log('===============================================================================================')
        reject("服务器连接异常，请检查网络再试");
      }
    });
  });
}
module.exports = {
  request,
  get,
  post
};