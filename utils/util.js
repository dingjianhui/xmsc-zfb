const formatTime = (date, sign = '/') => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  return [year, month, day].map(formatNumber).join(sign) + ' ' + [hour, minute, second].map(formatNumber).join(':');
};
const formatTimeMinute = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute].map(formatNumber).join(':');
};
const formatTimeSerHome = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  //   return [ month, day].map(formatNumber).join('-') + ' ' + [hour, minute].map(formatNumber).join(':')
  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute].map(formatNumber).join(':');
};
const formatTimedefault = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  //   return [ month, day].map(formatNumber).join('-') + ' ' + [hour, minute].map(formatNumber).join(':')
  return [year, month, day].map(formatNumber).join('-');
};
const formatEndTime = date => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  //   return [ month, day].map(formatNumber).join('-') + ' ' + [hour, minute].map(formatNumber).join(':')
  return [year, month, day + 1].map(formatNumber).join('-');
};
const getWeekByDate = dates => {
  let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
  let date = new Date(dates);
  date.setDate(date.getDate());
  let day = date.getDay();
  return show_day[day];
};
const formatNumber = n => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};
const formatSeconds = value => {
  let result = parseInt(value);
  let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600);
  let m = Math.floor(result / 60 % 60) < 10 ? '0' + Math.floor(result / 60 % 60) : Math.floor(result / 60 % 60);
  let s = Math.floor(result % 60) < 10 ? '0' + Math.floor(result % 60) : Math.floor(result % 60);
  let res = '';
  if (h !== '00') res += `${h}小时`;
  if (m !== '00') res += `${m}分`;
  res += `${s}秒`;
  return res;
};
const usedTimeToMiniute = value => {
  let result = parseInt(value);
  let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600);
  let m = Math.floor(result / 60 % 60) < 10 ? '0' + Math.floor(result / 60 % 60) : Math.floor(result / 60 % 60);
  let res = '';
  if (h !== '00') res += `${h}小时`;
  if (m !== '00') res += `${m}分`;
  if (res === '') res = `0分`;
  if (value <= 0) {
    res = `0分`;
  }
  return res;
};
const formatSecondsTimeNum = value => {
  let result = parseInt(value);
  let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600);
  let m = Math.floor(result / 60 % 60) < 10 ? '0' + Math.floor(result / 60 % 60) : Math.floor(result / 60 % 60);
  let s = Math.floor(result % 60) < 10 ? '0' + Math.floor(result % 60) : Math.floor(result % 60);
  let res = '';
  res = `${h}` + `:${m}` + `:${s}`;
  return res;
};
const timedifference = starttime => {
  var endtime = new Date().getTime();

  //计算天数 1s=1000ms
  var timediff = parseInt((endtime - starttime) / 1000);
  var days = parseInt(timediff / 86400);
  //计算小时数
  var remain = timediff % 86400;
  var hours = parseInt(remain / 3600);
  //计算分钟数
  var remain = remain % 3600;
  var mins = parseInt(remain / 60);
  let res = '';
  if (hours !== '00') res += `${hours}小时`;
  if (mins !== '00') res += `${mins}分`;
  return res;
};
const getQueryString = (url, name) => {
  var reg = new RegExp('(^|&|/?)' + name + '/([^&|/?]*)(&|/?|$)', 'i');
  // var reg = new RegExp('(^|&|/?)' + name + '=([^&|/?]*)(&|/?|$)', 'i')
  var r = url.substr(1).match(reg);
  if (r != null) {
    // console.log("r = " + r)
    // console.log("r[2] = " + r[2])
    return r[2];
  }
  return null;
};

// 秒转时间格式
const secondToTime = second => {
  let h = addZero(parseInt(second / 3600));
  let m = addZero(parseInt(second % 3600 / 60));
  let s = addZero(parseInt(second % 60));
  return {
    h,
    m,
    s
  };
};

// 补0
function addZero(val) {
  if (val < 10) return `0${val}`;
  return `${val}`;
}

// 蓝牙Mac地址解析
function BluetoothMac(val) {
  let list = val.split(':');
  let targetList = [];
  if (list.length < 4) return targetList;
  list = list.slice(list.length - 4);
  return ['' + list[0] + list[1], '' + list[2] + list[3]];
}

// 蓝牙通知 充电时长
function BluetoothPowerMinute(num) {
  if (!Number(num)) num = 0;
  let t = num.toString(16);
  if (t.length === 1) t = '000' + t;
  if (t.length === 2) t = '00' + t;
  if (t.length === 3) t = '0' + t;
  return t.toLocaleUpperCase();
}

// getDataByKey = (key) => {
//   return "token"
// }

function strToArrayBuffer(str) {
  let offset = 0;
  const buffer = new ArrayBuffer(str.length / 2);
  const dataView = new DataView(buffer);
  for (let i = 2; i < str.length + 2; i += 2) {
    dataView.setUint8(offset, '0x' + str.slice(i - 2, i));
    offset++;
  }
  return buffer;
}

// ArrayBuffer转16进度字符串示例
function ab2hex(buffer) {
  const hexArr = Array.prototype.map.call(new Uint8Array(buffer), function (bit) {
    return `00${bit.toString(16)}`.slice(-2);
  });
  return hexArr.join('');
}
function toTenDecimal(str) {
  let n = parseInt(str, 16).toString();
  return n.slice(n.length - 8);
}
const TITLE_NAME = '小电速充'
const COMPANY_NAME = '小电速兖'
const COMPANY_FULL_NAME = '湖南小电速充'
const FREE_TIME = 60
const AWARD_TIME = 600*3
const ZS_PHONE_NUMBER = '17707792723'
const IMG_URL = 'https://api.hnxmkj.net/static'
const PHONE_NUMBER = '4008678238'
const GM_PHONE_NUMBER = '4008678238'
const ZM_PHONE_NUMBER = '4008678238'
module.exports = {
  formatTime,
  formatTimeMinute,
  formatTimeSerHome,
  getQueryString,
  formatSeconds,
  usedTimeToMiniute,
  formatSecondsTimeNum,
  timedifference,
  secondToTime,
  BluetoothMac,
  BluetoothPowerMinute,
  // getDataByKey
  TITLE_NAME,
  COMPANY_NAME,
  COMPANY_FULL_NAME,
  PHONE_NUMBER,
  FREE_TIME,
  AWARD_TIME,
  getWeekByDate,
  formatTimedefault,
  formatEndTime,
  addZero,
  strToArrayBuffer,
  ab2hex,
  toTenDecimal,
  ZS_PHONE_NUMBER,
  IMG_URL,
  GM_PHONE_NUMBER,
  ZM_PHONE_NUMBER
};