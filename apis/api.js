const app = getApp();
module.exports = {
  CheckOrderStatus: 'order-check',
  // 检验订单状态
  OrderInfo: 'order-info',
  GetDeviceInfo: 'dev-info',
  // 获取设备信息
  GetRoomDeviceInfo: 'room-dev-info',
  // 获取设备信息
  GetNewDeviceInfo: 'device-info',
  // 获取设备信息
  GetPwd: 'dev-password',
  // 获取密码
  GetUseTime: 'dev-usetime',
  // 二次扫描页面 显示 的使用时间
  GetResetPwd: 'reset-dev-password',
  // 重置密码
  UploadWxFile: 'upload',
  // 文件上传
  ReportSubmit: 'report',
  // 故障申报
  GetOrders: 'orders',
  // 获取订单列表
  GetOrderDetail: 'order-detail',
  // 获取订单详情
  GetPriceDetail: 'get-price-detail',
  // 获取价格详情
  getPraiseDetail: 'get-goods-details',
  // 用户登录注册
  GetSessionKey: 'alipay/login',
  // code2session
  GetMemberInfo: 'member-info',
  // 获取会员信息
  UpdateMemberPhone: 'member-phone',
  // 更新会员手机号码

  // 押金支付相关
  OderCreate: 'alipay/create-charge-order',
  // 订单创建
  CloseOrder: 'refund',
  // 结束订单申请退款

  // 支付分支付相关
  ScoreOrderCreate: 'payscore-order',
  // 微信支付分订单创建
  ScoreOrderClose: 'order-complete',
  // 微信支付分订单创建

  // 日志相关
  QrLogs: 'logs-qr',
  // 扫描日志

  // code验证
  codeConfirm: 'roomVendingConfirm',
  // code验证

  // 订阅订单消息
  orderRemindRss: 'order-remind-rss',
  fixedRemindRss: 'fixed-remind-rss',
  getFixedRemindRss: 'get-fixed-remind-rss',
  // 开始计费时长更新
  orderAwardTime: 'award-time',
  // 清扫
  cleanInfo: 'notifications',
  // 工单列表
  workList: 'service-order-list',
  workDetails: 'service-order-details',
  //商品列表
  goodsList: 'goods-list',
  //商品详情
  goodsDetails: 'goods-details',
  //兑换商品提交
  getPrice: 'comment-submit',
  //地址列表
  addressList: 'address-list',
  //查询地址详情
  addressDetails: 'address-details',
  //新增地址
  addressCreate: 'address-create',
  //修改地址
  addressUpdate: 'address-update',
  commentDetails: 'comment-details',
  //加盟合作
  leagueSubmit: 'league-submit',
  //投诉建议提交
  complaintAdviceSubmit: 'complaint-advice',
  //查询酒店公告
  getPlaceBulletin: 'place-bulletin',
  // 酒店超市
  supermarketInfo: "get-goods-list",
  // 商品类型
  goodsType: "get-goods-type",
  // 创建商品订单
  createGoodsOrder: "alipay/create-goods-order",
  // 商品订单 重新支付
  againGoodsOrder: "alipay/again-goods-order",
  // 取消订单
  cancelGoodsOrder: "cancel-goods-order",
  // 查询商品订单
  getGoodsOrders: "get-goods-orders",
  // 查询商品详情
  getGoodsDetails: "get-goods-order-details",
  // 购物车查询列表
  getCartList: "get-goods-buy-cart-list",
  // 购物车 新增 更新
  updateCart: "set-goods-buy-cart",
  // 购物车删除商品
  deleteCart: "delete-goods-buy-cart",
  // 清空购物车
  clearCart: "empty-goods-buy-cart",
  // // 查询当前房间类型
  // getRoomTypeInfo: "get-room-type-info",
  // // 创建续住订单
  // createRoomOrder: "create-room-order",
  // 查询工单续住详情
  continuationDetails: "service-room-order-details",
  // 首页公告信息
  getNoticeInfo: "get-home-bulletin-list",
  // 公告列表
  getNoticeList: "get-all-bulletin-list",
  // 提交门店投诉建议
  submitReport: "place-report",
  // 查询当前房间类型
  getRoomTypeInfo: "get-room-type-info",
  // 创建续住订单
  createRoomOrder: "alipay/create-room-order",
  createBelowLineRoomOrder: "create-below-line-room-order",
  // 门店物品查询
  getServiceList: "get-places-service-materials-list",
  // 查询充电可用优惠券
  getUseChangeCouponList: "get-use-charge-coupon-list",
  // 删除地址
  deleteAddress: "address-delete",
  // 领取红包
  commentRedPacketReceive: "comment-red-packet-receive",
  commentRedPacketReceiveInform: "comment-red-packet-receive-inform",
  addScanLog: 'add-scan-log',
  // 扫码日志
  addServiceLog: 'add-service-log',
  // 服务点击日志
  notifications: 'notifications',
  addBluetoothRechargeLog: 'add-bluetooth-recharge-log',
  // 线充售货机
  automateGoodsList: 'automate/get-goods-list',
  // 查询售货机商品
  automateCreateGoodsOrder: 'alipay/create-automate-order',
  // 创建售货机订单
  automateShippingGoodsAdvice: 'automate/shipping-advice',
  // 售货机出货通知
  automateOrderList: 'automate/get-order-list',
  // 查询售货机订单

  // 往返到家点击日志
  addWyLog: 'add-wy-log',
  // 领取优惠券
  getCoupon: 'device/draw-coupon',
  // 盲盒抽奖相关
  getDrawGoodsList: 'get-draw-goods-list',
  setCommentGoods: 'set-comment-goods',
  getDrawGoodsPrizeList: 'get-draw-goods-prize-list',
  getDevPasswordVerify: 'dev-password-verify',

  // 会员信息、会员提现
  cashSubmit: 'member-cash-submit',
  memberInfoByOpenid: 'member-info-by-openid',
  // 获取商家信息
  placeUserInfo: 'user-info-by-openid'
};
