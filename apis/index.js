import wx from '../subwe/bridge';
import { Http } from '../utils/http.js';
const app = getApp();
const api = require('./api');
// const baseUrl = app.globalData.apiUrl
export class Index extends Http {
  constructor() {
    super();
  }
  // 扫描进入或跳转进入，先登录
  getLoginToken() {
    return new Promise((resolve, reject) => {
      my.getAuthCode({
        scopes: 'auth_user',
        success: res => {
          console.log('getAuthCode', res)
          wx.request({
            url: app.globalData.apiUrl + api.GetSessionKey,
            data: {authCode: res.authCode},
            method: "POST",
            header: {'Content-Type': 'application/json'},
            success(res) {
              if (res.statusCode === 200) {
                resolve(res.data);
              } else {
                wx.showToast({title: res.data.message, icon: "warn"});
              }
            },
            fail(res) {
              reject();
              console.log("error info", res);
            }
          });
        },
        fail: res => {
          reject();
          console.error('wx login fail', res)
        }
      });

      // wx.login({
      //   success(res) {
      //     wx.request({
      //       url: app.globalData.apiUrl + api.GetSessionKey,
      //       data: {
      //         code: res.code
      //       },
      //       method: "POST",
      //       header: {
      //         'Content-Type': 'application/json'
      //       },
      //       success(res) {
      //         if (res.statusCode == 200) {
      //           resolve(res.data);
      //         } else {
      //           wx.showToast({
      //             title: res.data.message,
      //             icon: "warn"
      //           });
      //         }
      //       },
      //       fail(res) {
      //         console.log("error info", res);
      //       }
      //     });
      //   },
      //   fail(err) {
      //     reject();
      //     console.error('wx login fail', err);
      //   }
      // });

      // wx.login({
      //   success(res) {
      //     wx.request({
      //       url: app.globalData.apiUrl + api.GetSessionKey,
      //       data: {
      //         code: res.code
      //       },
      //       method: "POST",
      //       header: {
      //         'Content-Type': 'application/json'
      //       },
      //       success(res) {
      //         if (res.statusCode == 200) {
      //           resolve(res.data);
      //         } else {
      //           wx.showToast({
      //             title: res.data.message,
      //             icon: "warn"
      //           });
      //         }
      //       },
      //       fail(res) {
      //         console.log("error info", res);
      //       }
      //     });
      //   },
      //   fail(err) {
      //     reject();
      //     console.error('wx login fail', err);
      //   }
      // });
    });
  }

  // 获取设备信息
  getDeviceInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetDeviceInfo,
      method: 'GET',
      data: data
    });
  }

  // 获取房间设备信息
  getRoomDeviceInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetRoomDeviceInfo,
      method: 'GET',
      data: data
    });
  }

  // 获取设备信息
  getNewDeviceInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetNewDeviceInfo,
      method: 'GET',
      data: data
    });
  }

  // 检查订单状态
  checkOrderStatus(data) {
    return this.request({
      url: app.globalData.apiUrl + api.CheckOrderStatus,
      method: 'POST',
      data: data
    });
  }
  GetOrderInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.OrderInfo,
      method: 'POST',
      data: data
    });
  }

  // 更新会员信息-授权后的会员信息
  updateMemberInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetMemberInfo,
      method: 'GET',
      data: data
    });
  }

  // 创建微信支付分订单
  createOrderByPayScore(data) {
    return this.request({
      url: app.globalData.apiUrl + api.ScoreOrderCreate,
      method: 'POST',
      data: data
    });
  }

  // 创建支付订单
  createOrderByDeposit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.OderCreate,
      method: 'POST',
      data: data
    });
  }

  // 获取密码
  getPassword(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetPwd,
      method: 'POST',
      data: data
    });
  }

  // 重置密码
  resetPassword(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetResetPwd,
      method: 'POST',
      data: data
    });
  }

  // 获取使用时长
  getUseTime(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetUseTime,
      method: 'POST',
      data: data
    });
  }

  // 订单结束
  closeOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.CloseOrder,
      method: 'POST',
      data: data
    });
  }

  // 获取订单列表
  getOrderList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetOrders,
      method: 'GET',
      data: data
    });
  }

  // 故障申请
  ReportSubmit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.ReportSubmit,
      method: 'POST',
      data: data
    });
  }

  // code验证
  codeConfirm(data) {
    return this.request({
      url: app.globalData.apiUrl + api.codeConfirm,
      method: 'POST',
      data: data
    });
  }
  GetPriceDetail(data) {
    return this.request({
      url: app.globalData.apiUrl + api.GetPriceDetail,
      method: 'POST',
      data: data
    });
  }
  updateMemberPhone(data) {
    return this.request({
      url: app.globalData.apiUrl + api.UpdateMemberPhone,
      method: 'POST',
      data: data
    });
  }

  // 订阅订单消息
  orderRemindRss(data) {
    return this.request({
      url: app.globalData.apiUrl + api.orderRemindRss,
      method: 'POST',
      data: data
    });
  }
  fixedRemindRss(data) {
    return this.request({
      url: app.globalData.apiUrl + api.fixedRemindRss,
      method: 'POST',
      data: data
    });
  }
  getFixedRemindRss(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getFixedRemindRss,
      method: 'POST',
      data: data
    });
  }

  // 开始计费时长更新
  orderAwardTime(data) {
    return this.request({
      url: app.globalData.apiUrl + api.orderAwardTime,
      method: 'POST',
      data: data
    });
  }

  // 清扫
  cleanFormSubmit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.cleanInfo,
      method: 'POST',
      data: data
    });
  }

  // 获取工单位列表
  getWorkList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.workList,
      method: 'GET',
      data: data
    });
  }
  // 获取工单详情
  getWorkDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.workDetails,
      method: 'POST',
      data: data
    });
  }

  //获取商品详情
  getGoodsDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.goodsDetails,
      method: 'POST',
      data: data
    });
  }

  //获取商品列表
  getGoodsList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.goodsList,
      method: 'GET',
      data: data
    });
  }
  //兑换商品提交
  getPrice(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getPrice,
      method: 'post',
      data: data
    });
  }

  //地址列表
  addressList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addressList,
      method: 'POST',
      data: data
    });
  }

  //查询地址详情
  getAddressDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addressDetails,
      method: 'POST',
      data: data
    });
  }

  //新增地址
  addressCreate(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addressCreate,
      method: 'POST',
      data: data
    });
  }

  //修改地址
  addressUpdate(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addressUpdate,
      method: 'POST',
      data: data
    });
  }

  // 评论详情
  commentDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.commentDetails,
      method: 'POST',
      data: data
    });
  }
  // 合作加盟
  leagueSubmit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.leagueSubmit,
      method: 'POST',
      data: data
    });
  }

  // 投诉建议提交
  complaintAdviceSubmit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.complaintAdviceSubmit,
      method: 'POST',
      data: data
    });
  }

  //查询酒店公告
  getPlaceBulletin(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getPlaceBulletin,
      method: 'POST',
      data: data
    });
  }

  // 查询商品客房信息
  supermarketInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.supermarketInfo,
      method: 'POST',
      data: data
    });
  }
  // 获取商品类型
  getGoodsType(data) {
    return this.request({
      url: app.globalData.apiUrl + api.goodsType,
      method: 'POST',
      data: data
    });
  }
  // 创建商品订单
  createGoodsOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.createGoodsOrder,
      method: 'POST',
      data: data
    });
  }
  // 商品订单 重新支付
  againGoodsOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.againGoodsOrder,
      method: 'POST',
      data: data
    });
  }
  // 取消订单
  cancelGoodsOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.cancelGoodsOrder,
      method: 'POST',
      data: data
    });
  }
  // 查询商品订单
  getGoodsOrders(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getGoodsOrders,
      method: 'POST',
      data: data
    });
  }
  // 查询商品详情
  getGoodsDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getGoodsDetails,
      method: 'POST',
      data: data
    });
  }
  // 购物车查询列表
  getCartList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getCartList,
      method: 'POST',
      data: data
    });
  }
  // 购物车 新增 更新
  updateCart(data) {
    return this.request({
      url: app.globalData.apiUrl + api.updateCart,
      method: 'POST',
      data: data
    });
  }
  // 购物车删除商品
  deleteCart(data) {
    return this.request({
      url: app.globalData.apiUrl + api.deleteCart,
      method: 'POST',
      data: data
    });
  }
  // 清空购物车
  clearCart(data) {
    return this.request({
      url: app.globalData.apiUrl + api.clearCart,
      method: 'POST',
      data: data
    });
  }
  getRoomTypeInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getRoomTypeInfo,
      method: 'POST',
      data: data
    });
  }
  // 好评详情
  getPraiseDetail(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getPraiseDetail,
      method: 'POST',
      data: data
    });
  }
  createRoomOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.createRoomOrder,
      method: 'POST',
      data: data
    });
  }
  createBelowLineRoomOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.createBelowLineRoomOrder,
      method: 'POST',
      data: data
    });
  }
  // 查询工单续住详情
  continuationDetails(data) {
    return this.request({
      url: app.globalData.apiUrl + api.continuationDetails,
      method: 'POST',
      data: data
    });
  }
  // 查询首页公告信息
  getNoticeInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getNoticeInfo,
      method: 'POST',
      data: data
    });
  }
  // 查询公告列表
  getNoticeList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getNoticeList,
      method: 'POST',
      data: data
    });
  }
  // 提交门店投诉建议
  submitReport(data) {
    return this.request({
      url: app.globalData.apiUrl + api.submitReport,
      method: 'POST',
      data: data
    });
  }
  // 查询当前房间类型
  getRoomTypeInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getRoomTypeInfo,
      method: 'POST',
      data: data
    });
  }
  // 创建续住订单
  createRoomOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.createRoomOrder,
      method: 'POST',
      data: data
    });
  }
  // 门店物品查询
  getServiceList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getServiceList,
      method: 'POST',
      data: data
    });
  }
  // 门店物品查询
  getUseChangeCouponList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getUseChangeCouponList,
      method: 'POST',
      data: data
    });
  }
  // 删除地址
  deleteAddress(data) {
    return this.request({
      url: app.globalData.apiUrl + api.deleteAddress,
      method: 'POST',
      data: data
    });
  }

  // 领取红包
  commentRedPacketReceive(data) {
    return this.request({
      url: app.globalData.apiUrl + api.commentRedPacketReceive,
      method: 'POST',
      data: data
    });
  }
  commentRedPacketReceiveInform(data) {
    return this.request({
      url: app.globalData.apiUrl + api.commentRedPacketReceiveInform,
      method: 'POST',
      data: data
    });
  }
  notifications(data) {
    return this.request({
      url: app.globalData.apiUrl + api.notifications,
      method: 'POST',
      data: data
    });
  }
  addBluetoothRechargeLog(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addBluetoothRechargeLog,
      method: 'POST',
      data: data
    });
  }
  getAutomateGoodsList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.automateGoodsList,
      method: 'POST',
      data: data
    });
  }
  automateCreateGoodsOrder(data) {
    return this.request({
      url: app.globalData.apiUrl + api.automateCreateGoodsOrder,
      method: 'POST',
      data: data
    });
  }
  automateShippingGoodsAdvice(data) {
    return this.request({
      url: app.globalData.apiUrl + api.automateShippingGoodsAdvice,
      method: 'POST',
      data: data
    });
  }
  getAutomateOrderList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.automateOrderList,
      method: 'POST',
      data: data
    });
  }
  addWyLog(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addWyLog,
      method: 'POST',
      data: data
    });
  }
  addServiceLog(data) {
    return this.request({
      url: app.globalData.apiUrl + api.addServiceLog,
      method: 'POST',
      data: data
    });
  }
  getCoupon(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getCoupon,
      method: 'POST',
      data: data
    });
  }
  getDrawGoodsList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getDrawGoodsList,
      method: 'POST',
      data: data
    });
  }
  getDrawGoodsPrizeList(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getDrawGoodsPrizeList,
      method: 'POST',
      data: data
    });
  }
  setCommentGoods(data) {
    return this.request({
      url: app.globalData.apiUrl + api.setCommentGoods,
      method: 'POST',
      data: data
    });
  }
  getDevPasswordVerify(data) {
    return this.request({
      url: app.globalData.apiUrl + api.getDevPasswordVerify,
      method: 'POST',
      data: data
    });
  }

  GetMemberInfoByOpenid(data) {
    return this.request({
      url: app.globalData.apiUrl + api.memberInfoByOpenid,
      method: 'GET',
      data: data
    });
  }

  WithdrawalSubmit(data) {
    return this.request({
      url: app.globalData.apiUrl + api.cashSubmit,
      method: 'POST',
      data: data
    });
  }

  GetPlaceUserInfo(data) {
    return this.request({
      url: app.globalData.apiUrl + api.placeUserInfo,
      method: 'GET',
      data: data
    })
  }

}
