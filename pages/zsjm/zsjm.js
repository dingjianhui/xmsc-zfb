import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import util from '../../utils/util.js';
const app = getApp();
WXPage({
  data: {
    phoneNumber: '',
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    phoneList: [{
      name: "李先生: ",
      phone: "17607709666",
      key: 1
    }, {
      name: "余先生: ",
      phone: "15392302111",
      key: 2
    }, {
      name: "陈先生: ",
      phone: "18565781911",
      key: 3
    }]
  },
  callPhone1(e) {
    // wx.reportEvent("zs_phone_call", {});
    wx.makePhoneCall({
      phoneNumber: '17350336235'
    });
  },
  callPhone2(e) {
    // wx.reportEvent("zs_phone_call", {});
    wx.makePhoneCall({
      phoneNumber: '17028236224'
    });
  },
  onLoad(options) {}
});
