import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { openBluetoothAdapter } from '../../utils/bluetooth';
import { Index } from '../../apis/index.js';
import util from '../../utils/util.js';
import CONST from '../../utils/const';
const app = getApp();
const API = new Index();
const plugin = requirePlugin('myPlugin');
WXPage({
  data: {
    CONST,
    userInfo: {},
    isPayScore: true,
    isCtrip: false,
    deviceSn: "",
    ctripCode: "",
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    loginInfo: {},
    // 登录信息
    devInfo: {},
    // 设备信息
    ruleInfo: [],
    // 设备所属套餐信息
    preOrderId: '',
    productId: '',
    tradeInfo: {},
    bannerList: [],
    feeMode: 0,
    idx: 0,
    isDisable: false,
    loading: false,
    adShow: false,
    showAd: 1,
    desc: '',
    priceInfo: null,
    bluetoothExist: false,
    bluetoothToPage: false,
    showPage: false,
    couponList: [],
    checkedCoupon: "",
    isZm: true,
    isBlue: false,
    imageUrl: `${app.globalData.imageUrl}`,
    bluetoothStatus: CONST.BluetoothStatusSearch,
    bluetoothErr: {}
  },
  // 页面加载
  onLoad: async function () {
    setTimeout(ev => this.setData({
      showPage: true
    }), 2800);
    this.setData({
      loginInfo: wx.getStorageSync('login_key'),
      loading: true,
      isPayScore: app.globalData.isPayScore,
      isCtrip: app.globalData.isCtrip,
      deviceSn: app.globalData.deviceSn,
      ctripCode: app.globalData.code,
      isZm: app.globalData.isZm,
      isBlue: app.globalData.isBlue
    });
    console.log('app-data',app)
    await API.getLoginToken().then(res => {
      // 登录
      this.setData({
        loginInfo: res.result
      });
      wx.setStorageSync("login_key", res.result);
    });
    if (app.globalData.isCtrip) {
      // wx.uma.setOpenid(this.data.loginInfo.openid);
    }
    if (!app.globalData.deviceSn || !app.globalData.apiUrl) {
      // 必须要有设备号，按钮才能正常点击
      this.setData({
        adShow: false
      });
      wx.setStorageSync("addShow", false);
      wx.showToast({
        icon: 'error',
        title: '未识别到设备'
      });
      return  wx.navigateTo({
        url: '/pages/serviceHome/serviceHome'
      });
      return;
    } else {
      // 同步获取设备信息
      await API.getDeviceInfo({
        "deviceSn": app.globalData.deviceSn
      }).then(res => {
        console.log('getDeviceInfo', res)
        if (res.result.placeId > 0) {
          let idx = -1;
          app.setBluetoothMac(res.result.bluetoothMac);
          app.setBluetoothType(res.result);
          this.setData({
            bannerList: res.result.banners ? JSON.parse(res.result.banners) : [],
            devInfo: res.result,
            ruleInfo: res.result.ruleInfo,
            feeMode: res.result.feeMode,
            idx: idx,
            showAd: res.result.isAd
          });
          console.log('this.setData', this)
          wx.setStorageSync("showAd", res.result.isAd);
          if (!res.result.id || !res.result.placeId) this.setData({
            isDisable: true
          });
        } else {
          this.setData({
            adShow: false
          });
          wx.setStorageSync("addShow", false);
          wx.showToast({
            icon: 'error',
            title: '该设备未绑定商户'
          });
          return wx.navigateTo({
            url: '/pages/serviceHome/serviceHome'
          });
        }
      });
    }
    console.log('openBluetoothAdapter-01', this.data.devInfo.bluetoothMac)
    if (this.data.devInfo.isSteward === 1 && this.data.devInfo.room) if (!app.toPageServiceHome()) return;
    if(this.data.devInfo.bluetoothMac)this.openBluetoothAdapter(this.data.devInfo.bluetoothMac);
    this.voiceBroadcast();
    this.setData({
      showPage: true
    });

    // 检查是否有订单
    await this.checkOrderStatus();
    if (this.data.bluetoothToPage) return;
    const {
      ctripCode
    } = this.data;
    !ctripCode && this.getCouponInfo();
    // 携程用户优惠提示
    if (this.data.ctripCode !== '' && this.data.ctripCode && this.data.orderInfo.id === 0) {
      API.codeConfirm({
        "code": this.data.ctripCode
      }).then(res => {
        this.getCouponInfo();
        const result = res.result;
        // 默认领取了优惠券
        if (result.receiveStatus === 1) {
          // 1的时候使用modal
          var discounts = '';
          if (result.discounts > 0) {
            discounts = '充电立省' + result.discounts / 100 + '元';
          }
          // wx.showModal({
          //   title: '小电温馨提示',
          //   content: result.firstRemark + discounts,
          //   showCancel: false,
          //   success(res) { }
          // })
        } else {
          // 0 2 3的时候使用toast
          wx.showToast({
            title: result.firstRemark
          });
        }
      });
    }
    console.log(this.data.showAd, this.data.orderInfo, this.data.orderInfo.id);
    if (this.data.showAd === 1 && this.data.loginInfo.state !== 2 && (!this.data.orderInfo.id || this.data.orderInfo.id === 0)) {
      // 在页面中定义插屏广告
      let interstitialAd = null;

      // 在页面onLoad回调事件中创建插屏广告实例
      // if (wx.createInterstitialAd) {
      //   interstitialAd = wx.createInterstitialAd({
      //     adUnitId: 'adunit-70d87ed7cfcc674e'
      //   });
      //   interstitialAd.onLoad(() => {});
      //   interstitialAd.onError(err => {});
      //   interstitialAd.onClose(() => {});
      // }

      // 在适合的场景显示插屏广告
      if (interstitialAd) {
        interstitialAd.show().catch(err => {
          console.error(err);
        });
      }
    }
    this.setData({
      loading: false,
      adShow: true
    });
    wx.setStorageSync("addShow", true);
  },

  requestSubscribeMessageFn() {
    my.requestSubscribeMessage({
      entityIds: ['2f2dcf2f100440328bf97aa049e0e02c'],
      complete: function (ev) {
        console.log('requestSubscribeMessage', ev)
      }
    })
  },

  openBluetoothAdapter(bluetoothMac) {
    console.log('openBluetoothAdapter-11', bluetoothMac)
    this.setData({
      bluetoothStatus: CONST.BluetoothStatusSearch
    });
    openBluetoothAdapter({
      bluetoothMac,
      deviceSn: this.data.devInfo.deviceSn,
      success: res => {
        this.setData({
          bluetoothStatus: CONST.BluetoothStatusSuccess
        });
      },
      fail: err => {
        console.log(err);
        this.setData({ bluetoothStatus: CONST.BluetoothStatusFail, bluetoothErr: err });
        // this.connectionBluetoothStatusOff()
      }
    });
  },
  connectionBluetoothStatusOff() {
    const bluetoothMac = this.data.devInfo.bluetoothMac
    if (bluetoothMac) {
      if (this.data.bluetoothStatus === CONST.BluetoothStatusSuccess) return true;
      wx.showModal({
        title: '蓝牙连接失败',
        content: `${this.data.bluetoothErr.errorMessage || '查看蓝牙连接教程'}`,
        confirmText: '查看教程',
        cancelText: '重新连接',
        cancelColor: '#333333',
        confirmColor: '#00A49A',
        success: (res)=> {
          if (res.confirm) wx.navigateTo({
            url: `/pages/padlineBlue/padline?showErr=1`
          });
        },
        complete: (res)=> {
          if(res.confirm || this.data.bluetoothStatus === CONST.BluetoothStatusSuccess) return
          console.log('手动重试', res, this.data.devInfo)
          this.openBluetoothAdapter(bluetoothMac)
        },
      });
      return false;
    }
    return true;
  },
  getCouponInfo() {
    console.log(this.data.devInfo);
    const {
      placeId,
      deviceSn
    } = this.data.devInfo;
    API.getUseChangeCouponList({
      placeId,
      deviceSn
    }).then(res => {
      res.result.forEach(i => {
        i.discounts = (i.discounts / 100).toFixed(0);
        if (i.endTime > 0) {
          let time = new Date(i.endTime * 1000);
          const year = time.getFullYear();
          const month = time.getMonth() + 1;
          const day = time.getDate();
          const hour = time.getHours();
          const minute = time.getMinutes();
          i.endTime = `${year}-${month}-${day} ${hour}:${minute}`;
        }
      });
      this.setData({
        couponList: res.result,
        checkedCoupon: res.result && res.result.length > 0 ? res.result[0].id : ""
        // .concat(res.result).concat(res.result).concat(res.result)
      });
    });
  },

  chooseCoupon(e) {
    const {
      checkedCoupon
    } = this.data;
    const id = Number(e.currentTarget.dataset.id);
    this.setData({
      checkedCoupon: checkedCoupon === id ? "" : id
    });
    console.log(this.data.checkedCoupon);
  },
  // 支付成功，页面跳转
  paySuccessToPage(trade_no) {
    if (!this.data.devInfo.bluetoothMac) return wx.redirectTo({
      url: '/pages/result/result?tradeno=' + trade_no
    });
    let mac = this.data.devInfo.bluetoothMac;
    let status = CONST.BluetoothOrderStatusPayment;
    let powerMinute = this.data.priceInfo.maxTime;
    if (this.data.feeMode === 2) powerMinute = this.data.priceInfo.useDuration;
    wx.redirectTo({
      url: `/pages/padlineBlue/padline?bluetoothMac=${mac}&status=${status}&powerMinute=${powerMinute}&tradeNo=${trade_no}`
    });
  },
  // 检查是否有订单
  async checkOrderStatus() {
    var param = {
      "deviceSn": app.globalData.deviceSn,
      "openid": this.data.loginInfo.openid
    };
    await API.checkOrderStatus(param).then(res => {
      const orderInfo = res.result;
      this.setData({
        orderInfo: orderInfo
      });
      if (orderInfo && orderInfo.state === 2) {
        this.setData({
          isDisable: true
        });
        var that = this;
        wx.showModal({
          title: '小电温馨提示',
          content: '您有一笔正在进行中的订单',
          confirmText: '立即进入',
          showCancel: false,
          success(res) {
            if (res.confirm) {
              wx.redirectTo({
                url: '/pages/cuse/cuse?devicesn=' + that.data.deviceSn
              });
            }
          }
        });
      }
    });
  },
  changeDeposit: function () {
    this.setData({
      isDeposit: true
    });
  },
  // 订单流程 -- 使用押金支付方式
  payDepositWay: function () {
    // 生成订单 -- 并创建微信预支付订单
    this.createOrder();
  },
  // 创建订单 -- 押金支付
  createOrder: async function () {
    if (!this.connectionBluetoothStatusOff()) return console.log('connectionBluetoothOff err');
    if (!this.data.devInfo.placeId || !this.data.devInfo.ruleInfo || !this.data.devInfo.ruleInfo.length) return wx.showModal({
      title: '提示',
      content: "设备未绑定使用，请联系工作人员",
      showCancel: false
    });
    // 判断登录的用户是否是拉黑状态
    if (this.data.loginInfo.state === 2) {
      return wx.showModal({
        title: '提示',
        content: "系统维护中，造成部分用户无法使用!",
        showCancel: false
      });
    }
    if (this.data.isDisable) return;
    this.setData({
      isDisable: true
    });
    var productId = '';
    if (this.data.feeMode === 2) {
      if (this.data.idx === -1) {
        wx.showModal({
          title: '提示',
          content: "请选择套餐模式",
          showCancel: false,
          success(res) {}
        });
        this.setData({
          isDisable: false
        });
        return;
      } else {
        productId = this.data.ruleInfo[this.data.idx].productId;
        this.setData({
          priceInfo: this.data.ruleInfo[this.data.idx]
        });
      }
    } else {
      productId = this.data.ruleInfo[0].productId;
      this.setData({
        priceInfo: this.data.ruleInfo[0]
      });
    }
    let param = {
      "deviceSn": this.data.devInfo.deviceSn,
      "productId": productId,
      "openid": this.data.loginInfo.openid,
      "feeMode": this.data.feeMode
    };
    const {
      checkedCoupon
    } = this.data;
    if (checkedCoupon) param.couponInfoId = checkedCoupon;
    // console.log('调用押金支付- -统一下订单，提交的参数', param)
    API.createOrderByDeposit(param).then(res => {
      if (res.code === 100200) {
        this.wxPayment(res.result, this.data.devInfo.deviceSn, productId); // 发起支付
        // } else if (res.code === 100006) {
        //   wx.showModal({
        //     title: '提示',
        //     content: "系统维护中，造成部分用户无法使用!",
        //     showCancel: false,
        //     success(res) { }
        //   })
        //   return
      } else if (res.code === 100007) {
        // 订单已存在
        const orderinfo = res.result;
        wx.redirectTo({
          url: '/pages/cuse/cuse?devicesn=' + orderinfo.deviceSn
        });
        return;
      } else {
        wx.redirectTo({
          url: '/pages/serviceHome/serviceHome'
        });
        return;
      }
    });
  },
  // 发起支付 -- 押金支付
  wxPayment: function (data, deviceSn, productId) {
    if (!this.data.devInfo.placeId || !this.data.devInfo.ruleInfo || !this.data.devInfo.ruleInfo.length) return wx.showModal({
      title: '提示',
      content: "设备未绑定使用，请联系工作人员",
      showCancel: false
    });

    let that = this
    let { out_trade_no, trade_no } = data.AliPayTradeCreateResBody || {}
    my.tradePay({
      tradeNO: out_trade_no,
      success: (res) => {
        console.log('tradePay-success', res)
        if(Number(res.resultCode) === 9000) return that.paySuccessToPage(trade_no)
        wx.showToast({title: res.memo || '支付失败', icon: 'none'})
        that.setData({isDisable: false})
      },
      fail: (res) => {
        console.log('tradePay-err', res)
        that.setData({isDisable: false})
        wx.showToast({title: '支付失败', icon: 'none'})
      }
    });

    // wx.showToast({title: '设备未绑定使用，请联系工作人员', icon: 'none'})
    // var that = this;
    // var param = JSON.parse(data);
    // wx.requestPayment({
    //   'timeStamp': param.timeStamp,
    //   'nonceStr': param.nonceStr,
    //   'package': param.package,
    //   'signType': param.signType,
    //   'paySign': param.paySign,
    //   'success': function (res) {
    //     that.paySuccessToPage(param.tradeNo);
    //     that.setData({
    //       isDisable: false
    //     });
    //   },
    //   'fail': function (res) {
    //     console.log("支付失败");
    //   },
    //   'complete': function (res) {
    //     that.setData({
    //       isDisable: false
    //     });
    //   }
    // });
  },
  // 押金租借
  depositLease() {
    this.createOrder();
  },

  alipayScopeWay() {
    plugin.startService({
      type: 'pay_after',
      sign_str: 'app_id=2021002163609520&biz_content=%7B%22category_id%22%3A%22credit_pay_after_use%22%2C%22out_agreement_no%22%3A%22ZM20230413100600897104%22%2C%22zm_service_id%22%3A%222020120500000000000074657200%22%7D&charset=utf-8&format=JSON&method=zhima.credit.payafteruse.creditagreement.sign&notify_url=https%3A%2F%2Fzltest.cnman.cn%2Fwxtoalipayapi%2Fv1%2Fwx%2Falipay%2FpayNotify%2Fcredit&sign=lQBNnGJO2JuBywqZbWDNF4P%2Bm6p8HIN4T0RfCi8yezhl4PZQWjvj5rAusy2IV95%2F%2F0MSsbf0KmQZS1uqK5laJIiS%2FH4Dxika%2BzXD6YEUCLk%2FUsTVrJAVc2X9sLrSMLvYX%2B70Kj9PVh4uGpL%2BUjgLnybF9p4R7MrlA46pGNnYHJeRR9MFGWACZxWnkt0z28niz6vxgvYG5A0HBCJBGQoa81yG8Az2MjLGYQYgzpEz4KDcGjj0unlVchWkvT%2BGIIaqyYjLCkgmjb%2Fo4xVeXPcm8ZhTj515wy1DBzsTZVkYsFUVyXO0fmbRgft8jrqfaB1e1%2FOoH82OfaxLGf9aoxH%2FwQ%3D%3D&sign_type=RSA2&timestamp=2023-04-13+10%3A06%3A00&version=1.0',
      zm_service_id: '2020120500000000000074657200',
      success: () => {},
      fail: () => {},
      complete: (ev) => {
        console.log('alipayScopeWay', ev)
      },
    })
  },

  // 订单流程 -- 使用支付分方式
  payScopeWay: async function () {
    if (!this.connectionBluetoothStatusOff()) return console.log('connectionBluetoothOff err');
    if (!this.data.devInfo.placeId || !this.data.devInfo.ruleInfo || !this.data.devInfo.ruleInfo.length) return wx.showModal({
      title: '提示',
      content: "设备未绑定使用，请联系工作人员",
      showCancel: false
    });
    console.log(this.data.loginInfo.state);
    // 判断登录的用户是否是拉黑状态
    if (this.data.loginInfo.state === 2) {
      return wx.showModal({
        title: '提示',
        content: "系统维护中，造成部分用户无法使用!",
        showCancel: false
      });
    }
    if (this.data.isDisable) return;
    this.setData({
      isDisable: true
    });
    var productId = '';
    if (this.data.feeMode === 2) {
      if (this.data.idx === -1) {
        wx.showModal({
          title: '提示',
          content: "请选择套餐模式",
          showCancel: false,
          success(res) {}
        });
        this.setData({
          isDisable: false
        });
        return;
      } else {
        productId = this.data.ruleInfo[this.data.idx].productId;
        this.setData({
          priceInfo: this.data.ruleInfo[this.data.idx]
        });
      }
    } else {
      productId = this.data.ruleInfo[0].productId;
      this.setData({
        priceInfo: this.data.ruleInfo[0]
      });
    }
    var deviceSn = app.globalData.deviceSn;
    let param = {
      "deviceSn": deviceSn,
      "productId": productId,
      "openid": this.data.loginInfo.openid,
      "feeMode": this.data.feeMode
    };
    const {
      checkedCoupon
    } = this.data;
    if (checkedCoupon) param.couponInfoId = checkedCoupon;
    API.createOrderByPayScore(param).then(res => {
      if (res.code === 100200) {
        this.payScoreUse(res.result, deviceSn, productId); // 跳转微信授权页面
        // } else if (res.code === 100006) {
        //   wx.showModal({
        //     title: '提示',
        //     content: "系统维护中，造成部分用户无法使用!",
        //     showCancel: false,
        //     success(res) { }
        //   })
        //   return
      } else if (res.code === 100007) {
        const orderinfo = res.result;
        wx.redirectTo({
          url: '/pages/cuse/cuse?devicesn=' + orderinfo.deviceSn
        });
        return;
      } else {
        wx.redirectTo({
          url: '/pages/padline/padline'
        });
        return;
      }
    });
  },
  // 确认订单
  confirmOrder: function (tradeno) {
    var that = this;
    wx.showLoading({
      title: '订单确认中'
    });
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    var setinterval = setInterval(function () {
      var timestamp1 = Date.parse(new Date());
      timestamp1 = timestamp1 / 1000;
      API.GetOrderInfo({
        "tradeNo": tradeno
      }).then(res => {
        if (res.result.state === 2) {
          wx.hideLoading();
          that.paySuccessToPage(tradeno);
          clearInterval(setinterval);
        }
      });
      console.log(timestamp, timestamp1);
      if (timestamp1 - timestamp >= 8) {
        that.setData({
          isDisable: false
        });
        wx.hideLoading();
        wx.showToast({
          title: '暂无订单',
          icon: 'none',
          duration: 2000
        });
        clearInterval(setinterval);
      }
    }, 1000);
  },
  // 微信支付分创建成功后 - 跳转
  payScoreUse: function (data, deviceSn, productId) {
    var that = this;
    let param = JSON.parse(data);
    // if (wx.openBusinessView) {
    //   wx.openBusinessView({
    //     businessType: 'wxpayScoreUse',
    //     extraData: {
    //       mch_id: param.mch_id,
    //       package: param.package,
    //       timestamp: param.timestamp,
    //       nonce_str: param.nonce_str,
    //       sign_type: param.sign_type,
    //       sign: param.sign
    //     },
    //     success() {
    //       if (param.trade_no) {
    //         app.globalData.tradeNo = param.trade_no;
    //         that.confirmOrder(param.trade_no);
    //       }
    //     },
    //     fail(res) {
    //       console.log("fail", res);
    //       that.setData({
    //         isDisable: false
    //       });
    //     }
    //   });
    // } else {
    //   //引导用户升级微信版本
    //   wx.showModal({
    //     title: '提示',
    //     content: '微信版本太低，使用押金支付',
    //     success(res) {
    //       if (res.confirm) {
    //         console.log('用户点击确定使用押金支付');
    //         that.createOrder();
    //       } else if (res.cancel) {
    //         console.log('用户点击取消押金支付');
    //       }
    //     }
    //   });
    // }
  },
  // 语音播报
  voiceBroadcast() {
    if (this.data.devInfo.bluetoothMac == null) {
      this.setData({
        isDisable: false
      });
      // wx.playBackgroundAudio({
      //   dataUrl: 'https://zmapi.cnman.cn/padline.mp3'
      // });
    }
  },
  onShow: function () {},
  // 联系客服
  openService: function () {
    let phone = this.data.devInfo.prodType !== 1 ? util.ZM_PHONE_NUMBER : util.PHONE_NUMBER;
    wx.showActionSheet({
      itemList: ['使用帮助', '联系客服'],
      success: function (res) {
        if (res.tapIndex === 0) {
          wx.navigateTo({
            url: '/pages/mine/help/help' // 使用帮助
          });
        }

        if (res.tapIndex === 1) {
          wx.makePhoneCall({
            phoneNumber: phone
          });
        }
      },
      fail: function (res) {
        console.log(res.errMsg);
      }
    });
  },
  // 选择套餐规则
  chooseRule: function (e) {
    let index = e.currentTarget.dataset.index;
    // console.log(index)
    this.setData({
      idx: index
    });
  },
  // 《委托扣款授权书》
  agreeAuthorize: function () {
    wx.navigateTo({
      url: '/pages/mine/authbook2/authbook2'
    });
  },
  // 《用户租借须知》
  usernotice: function () {
    wx.navigateTo({
      url: '/pages/mine/usernotice/usernotice'
    });
  },
  // 关注携程酒店公众号文章 跳转
  toCtrip: function () {
    // wx.uma.trackEvent('CtripBanner');
    // wx.reportEvent("ctrip_gzh", {});
    wx.navigateTo({
      url: '/pages/mine/ctrip/ctrip'
    });
  },
  // 跳转飞猪小程序
  toFeizhu: function () {
    // wx.reportEvent("feizhu_banner", {});
    wx.navigateToMiniProgram({
      appId: 'wx6a96c49f29850eb5',
      path: 'pages/hotel-search/searchlist/index?fpid=17016&ttid=12wechat000004650'
    });
  },
  // 招商加盟跳转
  toZm: function () {
    // wx.reportEvent("zm_zs_banner", {});
    wx.navigateTo({
      url: '/pages/zsjm/zsjm'
      // url: '/pages/wifizs/wifizs',
    });
  },

  toZmGzh: function () {
    // wx.reportEvent("freegzh", {});
    wx.navigateTo({
      url: '/pages/mine/zmGzh/zmgzh'
    });
  },
  codeConfirm: async function () {
    // 检验code
    var param = {
      "code": this.data.ctripCode
    };
    // console.log('检验code', param)
    await API.codeConfirm(param).then(res => {
      const result = res.result;
      wx.showModal({
        title: '提示',
        content: result.message,
        showCancel: false,
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定');
          } else if (res.cancel) {
            console.log('用户点击取消');
          }
        }
      });
    });
  },
  getPhoneNumber() {
    wx.getPhoneNumber({
      success: __res__ => {
        const e = {
          detail: __res__
        };
        var that = this;
        if (e.detail.encryptedData !== '' && e.detail.iv !== '') {
          // 授权手机号
          var param = {
            errMsg: e.detail.errMsg,
            encryptedData: e.detail.encryptedData,
            iv: e.detail.iv,
            openid: that.data.loginInfo.openid
          };
          API.updateMemberPhone(param).then(res => {
            console.log("手机号码更新成功");
          });
        }
        that.createOrder();
        
        // if (that.data.isDeposit) {
        //   that.setData({
        //     isDeposit: false
        //   });
        //   that.createOrder();
        //   return;
        // } else {
        //   if (that.data.feeMode === 1 || that.data.feeMode === 2 && that.data.isCtrip) {
        //     that.payScopeWay();
        //     return;
        //   }
        //   if (that.data.feeMode === 2 && !that.data.isCtrip) {
        //     that.createOrder();
        //     return;
        //   }
        // }

        // if (that.data.isCtrip) { // 携程用户
        //   that.payScopeWay()
        // } else {
        //   if (that.data.isPayScore) { // 免押
        //     that.payScopeWay()
        //   } else {
        //     that.createOrder()
        //   }
        // }
      }
    })
  },

  // codeConfirm: async function () {
  //   // 检验code
  //   var param = { "code": this.data.ctripCode }
  //   // console.log('检验code', param)
  //   await API.codeConfirm(param).then(res => {
  //     const result = res.result
  //     wx.showModal({
  //       title: '提示',
  //       content: result.message,
  //       showCancel: false,
  //       success(res) {
  //         if (res.confirm) {
  //           console.log('用户点击确定')
  //         } else if (res.cancel) {
  //           console.log('用户点击取消')
  //         }
  //       }
  //     })
  //   })
  // },
  toBack: function () {
    // wx.navigateBack({
    //   delta: 1
    // });
    wx.redirectTo({
      url: '/pages/serviceHome/serviceHome'
    });
  },
  zfbLink: function () {
    wx.redirectTo({
      url: '/pages/zfbLink/zfbLink'
    });
  },
  tohp: function () {
    // wx.reportEvent("event_haoping", {});
    wx.navigateTo({
      url: '/pages/evaluateGift/evaluateGift'
    });
  },
  toXm() {
    wx.navigateTo({
      url: '/pages/webview/xmNnWebview/xmNnWebview'
    });
  },
  // 足浴
  thirdPageGotoLavipeditum() {
    // wx.reportEvent("service_meituan_sanazuyu", {});
    if (this.data.devInfo.city === '武汉市') {
      wx.navigateTo({
        url: '/pages/webview/wyWebview/wyWebview'
      });
    } else if (this.data.devInfo.city === '衡阳市') {
      wx.navigateTo({
        url: '/pages/webview/eyWebview/eyWebview'
      });
    } else if (this.data.devInfo.city === '南宁市') {
      wx.navigateTo({
        url: '/pages/webview/xmNnWebview/xmNnWebview'
      });
    } else {
      wx.showToast({
        title: "目前该城市暂不支持",
        icon: "none"
      });
      return;
    }
  }
});
