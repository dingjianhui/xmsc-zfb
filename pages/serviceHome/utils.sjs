export var toFixed = function (num, n = 1) {
  return (num * n / 100).toFixed(2);
};