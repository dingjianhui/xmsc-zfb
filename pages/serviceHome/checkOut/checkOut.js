import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
var util = require('../../../utils/util');
var app = getApp();
import { Index } from '../../../apis/index.js';
const API = new Index();
WXPage({
  data: {
    notificationType: 1,
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    showAd: 1,
    username: "",
    phone: "",
    baseInfo: {},
    message: ""
  },
  onLoad(e) {
    this.setData({
      showAd: wx.getStorageSync('showAd'),
      baseInfo: wx.getStorageSync("baseInfo")
    });
  },
  // 输入备注
  getBeizhu(event) {
    this.setData({
      message: event.detail.value //获取属于框输入的文字
    });
  },

  // 姓名
  nameInput(event) {
    this.setData({
      username: event.detail.value
    });
  },
  // 手机号
  phoneInput(event) {
    this.setData({
      phone: event.detail.value
    });
  },
  formSubmit() {
    const {
      room,
      deviceSn,
      placeId,
      placeName
    } = this.data.baseInfo;
    const {
      phone,
      username,
      message
    } = this.data;
    let info = {
      phone,
      room,
      name: username
    };
    const param = {
      placeId,
      placeName,
      deviceSn,
      notificationType: 7,
      memberPhone: phone,
      placeRoom: room,
      info: JSON.stringify(info),
      makeTime: "1970-01-01",
      message
    };
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    API.notifications(param).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '提交成功！',
        icon: "none"
      }), setTimeout(() => {
        wx.redirectTo({
          url: '/pages/serviceHome/serviceHome'
        });
      }, 1500);
    });
  }
});