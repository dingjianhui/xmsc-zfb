import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
const app = getApp();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    imageUrl: `${app.globalData.imageUrl}/zhongman2`
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},
  toWangyue() {
    // wx.reportEvent("wangyue_yuyue_btn", {});
    wx.navigateTo({
      url: '/pages/webview/wyWebview/wyWebview'
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {}
});
