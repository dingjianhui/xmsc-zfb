import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
const app = getApp();
import { Index } from '../../apis/index';
const API = new Index();
// pages/league.js
WXPage({
  /**
   * 页面的初始数据
   */
  data: {},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},
  //合作加盟提交
  leagueSubmit: function () {
    let param = {
      "name": this.data.name,
      "phone": this.data.phone,
      "isAdmin": this.data.isAdmin,
      //合作方式
      "budget": this.data.budget,
      //投资预算
      "investmentRegion": this.data.investmentRegion,
      //投资区域
      "isCompany": this.data.isCompany,
      //是否为企业单位。1 企业 2个人
      "cooperationType": this.data.cooperationType,
      //合作类型 1 合作商模式 2门店模式
      "message": this.data.message //备注
    };

    API.leagueSubmit(param).then(res => {
      wx.showToast({
        title: '新增成功',
        duration: 2000
      });
    });
  }
});