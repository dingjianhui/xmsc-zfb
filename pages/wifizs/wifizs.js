import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import util from '../../utils/util.js';
const app = getApp();
WXPage({
  data: {
    phoneNumber: '',
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    phoneList: [{
      name: "李先生: ",
      phone: "18565781911",
      key: 1
    }, {
      name: "余先生: ",
      phone: "13699830720",
      key: 2
    }]
  },
  callPhone1(e) {
    // wx.reportEvent("zs_phone_call", {});
    wx.makePhoneCall({
      phoneNumber: '18565781911'
    });
  },
  callPhone2(e) {
    // wx.reportEvent("zs_phone_call", {});
    wx.makePhoneCall({
      phoneNumber: '13699830720'
    });
  },
  onLoad(options) {}
});
