import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
//获取应用实例
import { Index } from '../../apis/index.js';
const app = getApp();
const API = new Index();
const {
  baseUrl,
  yundianUrl
} = require("../../config");
const {
  getQueryString,
  TITLE_NAME,
  PHONE_NUMBER
} = require("../../utils/util");
WXPage({
  data: {
    CustomBar: app.globalData.CustomBar,
    StatusBar: app.globalData.StatusBar,
    actualHeight: app.globalData.windowH - 100 / 750 * app.globalData.windowW,
    // 高度
    contentHeight: 0,
    scale: 18,
    // 缩放级别，默认18，数值在0~18之间
    address: '',
    // 地址名
    longitude: 0,
    // 经度
    latitude: 0,
    // 纬度
    controls: [],
    // 地图控件组
    markers: [],
    // 地图标记组
    province: "",
    city: "",
    district: "",
    orderInfo: {},
    TITLE_NAME,
    PHONE_NUMBER,
    loginInfo: {},
    deviceSn: app.globalData.deviceSn,
    showAd: 1
  },
  onLoad: async function (options) {
    this.setData({
      contentHeight: app.globalData.windowH - this.data.CustomBar - 90,
      showAd: wx.getStorageSync("showAd"),
      loginInfo: wx.getStorageSync('login_key')
    });
    if (!this.data.loginInfo) {
      await API.getLoginToken().then(res => {
        this.setData({
          loginInfo: res.result
        });
        wx.setStorageSync("login_key", res.result);
      });
    }

    // 获取最新订单信息
    var param = {
      "deviceSn": app.globalData.deviceSn,
      "openid": this.data.loginInfo.openid
    };
    await API.checkOrderStatus(param).then(res => {
      app.globalData.deviceSn = res.result.deviceSn;
      this.setData({
        orderInfo: res.result
      });
    });
  },
  //点击头像 跳转到个人中心
  onHeadTap: function () {
    wx.navigateTo({
      url: '/pages/serviceHome/serviceHome'
    });
  },
  onScan() {
    wx.scanCode({
      success: res => {
        var result = res.result;
        let q = decodeURIComponent(result);
        app.globalData.deviceSn = getQueryString(q, "dev");
        let pcl = getQueryString(q, "pcl");
        if (q.includes('yundian') || pcl === '10041') {
          app.globalData.apiUrl = yundianUrl ? yundianUrl : 'https://gmzfb.cnman.cn/v1/wx/';
          app.globalData.isZm = false;
        } else if (q.includes('blue.chinaman.ink')) {
          // 二代线
          app.globalData.apiUrl = baseUrl ? baseUrl : 'https://sczfb.cnman.cn/v1/wx/';
          app.globalData.isBlue = true;
        } else {
          app.globalData.apiUrl = baseUrl ? baseUrl : 'https://sczfb.cnman.cn/v1/wx/';
        }
        console.log(res.result);
        wx.navigateTo({
          url: '/pages/padline/padline'
        });
      }
    });
  },
  // 联系客服
  openService: function () {
    wx.showActionSheet({
      itemList: ['使用帮助', '联系客服'],
      success: function (res) {
        if (res.tapIndex === 0) {
          wx.navigateTo({
            url: '/pages/mine/help/help' // 使用帮助
          });
        }

        if (res.tapIndex === 1) {
          wx.makePhoneCall({
            phoneNumber: PHONE_NUMBER
          });
        }
      },
      fail: function (res) {
        console.log(res.errMsg);
      }
    });
  },
  toOrderDetail: function () {
    wx.navigateTo({
      url: '/pages/cuse/cuse?deviceSn=' + this.data.orderInfo.deviceSn
    });
  },
  closeAd: function () {
    this.setData({
      showAd: 2
    });
  },
  toMy: function () {
    wx.navigateTo({
      url: '/pages/mine/mine'
    });
  },
  toShop: function () {
    wx.showModal({
      title: '提示',
      content: '附近门店',
      success(res) {
        if (res.confirm) {
          // console.log('用户点击确定')
          wx.navigateTo({
            url: '/pages/padline/padline'
          });
        } else if (res.cancel) {
          // console.log('用户点击取消')
        }
      }
    });
  }
});
