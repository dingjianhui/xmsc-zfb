import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { Index } from '../../apis/index.js';
import util from '../../utils/util';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    userInfo: wx.getStorageSync('login_key') || {},
    canIUseGetUserProfile: false,
    loginInfo: null,
    StatusBar: app.globalData.StatusBar || {},
    deviceSn: '',
    placeUserInfo: {},
    navList: [
      { name: "充电订单", icon: "userIcon1.png", path: "/pages/mine/order/order" },
      { name: "使用帮助", icon: "menuIcon3.png", path: "/pages/mine/help/help" },
        // {
        //   name: "我的钱包",
        //   icon: "icon_my_order@2x.png",
        //   path: "/pages/mine/balance/balance" },
      //  {
      //   name: "售货机订单",
      //   icon: "icon_my_order@2x.png",
      //   path: "/pages/automateOrder/automateOrder"
      // }
      // { name: "福利中心", icon: "icon_my_welfare@2x.png", path: "" },
      // { name: "借钱", icon: "icon_my_borrow@2x.png", path: ""},
    ],

    // data 备注勿删
    menuList: [
      // {
      // name: "好评有礼",
      // icon: "img_Coupon@2x.png",
      // path: "/pages/evaluateGift/evaluateGift",
      // isDeviceSn: true
    // }, {
    //   name: "工单列表",
    //   icon: "icon_my_work@2x.png",
    //   path: "/pages/serviceMy/workList/workList"
    // },
    // // { name: "联系客服", icon: "icon_my_service@2x.png", path: "customerService" },
    // {
    //   name: "超市订单",
    //   icon: "car_icon.png",
    //   path: "/pages/supermarket/supermarketOrder/supermarketOrder"
    // }, {
    //   name: "使用帮助",
    //   icon: "icon_my_help@2x.png",
    //   path: "/pages/mine/help/help"
    // }, {
    //   name: "关于我们",
    //   icon: "icon_my_about@2x.png",
    //   path: "/pages/serviceMy/serviceAbout/serviceAbout"
    // }
    // { name: "投诉意见", icon: "icon_my_about@2x.png", path: "/pages/report/complaintAdvice/complaintAdvice" },
    ],

    imageUrl: `${app.globalData.imageUrl}`,
    showAd: 1,
    devInfo: {},
    phone: util.ZS_PHONE_NUMBER,
    prodType: 1,
    serviceHome: ''
  },
  onReady() {
    my.hideBackHome();
  },
  onLoad: async function (options) {
    my.hideBackHome();
    console.log(`wx.getStorageSync('userInfo')`, wx.getStorageSync('login_key'))
    this.setData({ StatusBar: app.globalData.StatusBar })
    this.setData({ deviceSn: app.globalData.deviceSn })

    let {
      serviceHome
    } = options;
    this.setData({
      serviceHome
    });
    if (!this.data.loginInfo) {
      await API.getLoginToken().then(res => {
        this.setData({
          userInfo: res.result,
          showAd: wx.getStorageSync("showAd")
        });
        wx.setStorageSync("login_key", res.result);
      });
    } else {
      this.setData({
        userInfo: wx.getStorageSync('login_key')
      });
    }
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      });
    }
    if (app.globalData.deviceSn) {
      // 同步获取设备信息
      await API.getDeviceInfo({
        "deviceSn": app.globalData.deviceSn
      }).then(res => {
        this.setData({
          devInfo: res.result,
          showAd: res.result.isAd,
          prodType: res.result.prodType
        });
      });
    }
    await API.GetPlaceUserInfo({ "openid": this.data.userInfo.openid }).then(res => {
      this.setData({
        placeUserInfo: res.result
      });
    })
  },
  // getUserProfile(e) {
  //   // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
  //   // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
  //   wx.getUserProfile({
  //     desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
  //     success: (res) => {
  //       this.setData({
  //         userInfo: res.userInfo,
  //       })
  //       var { errMsg, rawData, userInfo, signature, encryptedData, iv } = res
  //       var param = { errMsg: errMsg, rawData: rawData, userInfo: userInfo, signature: signature, encryptedData: encryptedData, iv: iv, openid: this.data.loginInfo.openid }
  //       API.updateMemberInfo(param).then(res => {
  //         console.log("会员信息更新成功")
  //         wx.setStorageSync("userInfo", res.result)
  //         this.setData({
  //           userInfo: res.result
  //         })
  //       })
  //     }
  //   })
  // },
  // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
  // getUserInfo(e) {
  //   this.setData({
  //     userInfo: e.detail.userInfo
  //   })
  // },
  otherFnNav(e) {
    const item = e.currentTarget.dataset.item;
    if (item.path === "customerService") {
      // 联系客服
      wx.showModal({
        title: '呼叫服务',
        content: `客服联系热线：${util.PHONE_NUMBER}`,
        confirmText: '立即呼叫',
        cancelColor: '#666',
        confirmColor: '#00A49AFF',
        success: res => {
          if (res.confirm) {
            this.makePhoneCall(util.PHONE_NUMBER);
          } else if (res.cancel) {
            console.log('用户点击取消');
          }
        }
      });
    } else {
      wx.navigateTo({
        url: item.path
      });
    }
  },
  // 拨打电话
  makePhoneCall(phone) {
    wx.makePhoneCall({
      phoneNumber: phone,
      fail: err => {
        wx.showToast({
          title: '拨打电话失败',
          icon: 'none'
        });
      }
    });
  },
  gotoPlaceAdmin() {
    wx.navigateTo({
      url: '/pages/mine/placeinfo/placeinfo'
    });
  },
  // toWorkList
  toNav(e) {
    console.log('toNav', e)
    const path = e.target.dataset.path;
    if (path) {
      wx.navigateTo({
        url: path
      });
    } else {
      wx.showToast({
        title: '即将上线，敬请期待',
        icon: 'none'
      });
    }
  }
});
