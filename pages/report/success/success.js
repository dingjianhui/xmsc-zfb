import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
WXPage({
  /**
   * 页面的初始数据
   */
  data: {},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},
  toBack() {
    wx.navigateBack({
      delta: 2
    });
  }
});