import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { Index } from '../../apis/index';
const app = getApp();
const API = new Index();
const url = require('../../apis/api');

// pages/order/repairs/repairs.js
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    describe: '',
    roomNumber: '',
    imageList: [],
    deviceSn: '',
    deviceInfo: {},
    orderInfo: {}
  },
  onLoad: function (options) {
    this.setData({
      deviceSn: options.deviceSn
    });
    this.getDeviceInfo();
    this.checkOrderStatus();
  },
  // 查询设备详情
  getDeviceInfo() {
    API.getDeviceInfo({
      "deviceSn": this.data.deviceSn
    }).then(res => {
      this.setData({
        deviceInfo: res.result
      });
    });
  },
  // 查询订单详情
  checkOrderStatus() {
    let loginInfo = wx.getStorageSync('login_key');
    let param = {
      "deviceSn": this.data.deviceSn,
      "openid": loginInfo.openid
    };
    API.checkOrderStatus(param).then(res => {
      this.setData({
        orderInfo: res.result
      });
    });
  },
  // 上传图片
  uploadImgFileFn(tempFilePath, loginInfo) {
    wx.uploadFile({
      url: app.globalData.apiUrl + url.UploadWxFile,
      filePath: tempFilePath,
      name: 'file',
      formData: {
        'openid': loginInfo.openid
      },
      header: {
        'content-type': 'multipart/form-data',
        'Access-Token': 'dingo ' + loginInfo.token
      },
      success: res => {
        if (res.statusCode === 401) return wx.navigateTo({
          url: '/pages/padline/padline'
        });
        let data = JSON.parse(res.data);
        let imageList = this.data.imageList;
        imageList.push(data.result);
        this.setData({
          imageList
        });
      }
    });
  },
  // 选择异常类型
  onchange(e) {
    let type = e.currentTarget.dataset.type;
    this.setData({
      active: type
    });
  },
  changeVal(e) {
    let val = e.detail.value;
    this.setData({
      describe: val
    });
  },
  // 上传图片
  changeImage() {
    let that = this;
    wx.chooseImage({
      count: 4,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: res => {
        let loginInfo = wx.getStorageSync('login_key');
        for (let tempFilePath of res.tempFilePaths || []) this.uploadImgFileFn(tempFilePath, loginInfo);
        // tempFilePath可以作为img标签的src属性显示图片
        // const tempFilePaths = res.tempFilePaths
        // let imageList = that.data.imageList
        // imageList = imageList.concat(tempFilePaths)
        // that.setData({imageList})
      }
    });
  },

  // 删除照片
  deleteImg(e) {
    let index = e.currentTarget.dataset.i;
    let imageList = this.data.imageList;
    imageList.splice(index, 1);
    this.setData({
      imageList
    });
  },
  // 提交
  submit() {
    let active = this.data.active;
    let describe = this.data.describe;
    let roomNumber = this.data.roomNumber;
    let imageList = this.data.imageList;
    if (active == 0) {
      wx.showToast({
        title: '请选择异常类型',
        icon: 'none'
      });
      return false;
    }
    if (describe == '') {
      wx.showToast({
        title: '请输入反馈内容',
        icon: 'none'
      });
      return false;
    }
    if (roomNumber == '') {
      wx.showToast({
        title: '请输入房间号',
        icon: 'none'
      });
      return false;
    }
    if (imageList.length == 0) {
      wx.showToast({
        title: '请上传照片',
        icon: 'none'
      });
      return false;
    }
    let param = {
      type: parseInt(active),
      desc: describe,
      roomnum: roomNumber,
      imgPaths: JSON.stringify(imageList)
    };
    param.deviceSn = this.data.deviceSn;
    param.shareMode = this.data.deviceInfo.shareMode;
    param.placeId = this.data.orderInfo.placeId;
    param.placeName = this.data.orderInfo.placeName;
    param.deviceName = this.data.deviceInfo.deviceName;
    param.tradeNo = this.data.orderInfo.tradeNo;
    API.ReportSubmit(param).then(res => {
      wx.navigateTo({
        url: '/pages/report/success/success'
      });
    });
  }
});