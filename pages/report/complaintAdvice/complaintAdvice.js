import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index';
const app = getApp();
const API = new Index();
const url = require('../../../apis/api');
WXPage({
  data: {
    active: 0,
    describe: '',
    roomNumber: '',
    imageList: [],
    devInfo: {},
    adviseList: [{
      title: '"服务"类 意见反馈',
      key: 5
    }, {
      title: '"设备"类 意见反馈',
      key: 6
    }, {
      title: '"意见"类 意见反馈',
      key: 7
    }, {
      title: '"体验"类 意见反馈',
      key: 8
    }, {
      title: "其它",
      key: 4
    }],
    roomNumber: ""
  },
  onLoad: function (options) {
    // 获取房间号
    this.getroomNum();
    // 获取登录信息
    this.getLoginToken();
  },
  // 获取登录信息的手机号
  getLoginToken() {
    API.getLoginToken().then(res => {
      console.log(res);
      if (!res) return;
      let loginInfo = res.result;
      console.log(loginInfo);
      this.setData({
        phone: loginInfo.phone,
        openid: loginInfo.openid
      });
      wx.setStorageSync("login_key", loginInfo);
    });
  },
  // 获取房间号和设备号
  getroomNum() {
    let param = {
      "deviceSn": app.globalData.deviceSn
    };
    API.getDeviceInfo(param).then(res => {
      app.setBluetoothMac(res.result.bluetoothMac);
      this.setData({
        devInfo: res.result
      });
    });
  },
  // 上传图片
  uploadImgFileFn(tempFilePath, loginInfo) {
    wx.uploadFile({
      url: app.globalData.apiUrl + 'upload',
      filePath: tempFilePath,
      name: 'file',
      formData: {
        'openid': loginInfo.openid
      },
      header: {
        'content-type': 'multipart/form-data',
        'Access-Token': 'dingo ' + loginInfo.token
      },
      fail: res => {
        console.log(res);
      },
      success: res => {
        console.log("图片上传状态", res.statusCode);
        console.log("所有返回", res);
        if (res.statusCode === 401) return wx.navigateTo({
          url: '/pages/serviceHome/serviceHome'
        });
        let data = JSON.parse(res.data);
        console.log("图片数据", res.data);
        let imageList = this.data.imageList;
        imageList.push(data.result);
        this.setData({
          imageList
        });
      }
    });
  },
  // 上传图片
  changeImage() {
    let that = this;
    wx.chooseImage({
      count: 4,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: res => {
        let loginInfo = wx.getStorageSync('login_key');
        for (let tempFilePath of res.tempFilePaths || []) this.uploadImgFileFn(tempFilePath, loginInfo);
        // tempFilePath可以作为img标签的src属性显示图片
        // const tempFilePaths = res.tempFilePaths
        // let imageList = that.data.imageList
        // imageList = imageList.concat(tempFilePaths)
        // that.setData({imageList})
      }
    });
  },

  // 删除照片
  deleteImg(e) {
    let index = e.currentTarget.dataset.i;
    let imageList = this.data.imageList;
    imageList.splice(index, 1);
    this.setData({
      imageList
    });
  },
  // 选择投诉类型
  onchange(e) {
    let type = e.currentTarget.dataset.type;
    this.setData({
      active: type
    });
  },
  // 投诉内容
  changeVal(e) {
    this.setData({
      describe: e.detail.value
    });
  },
  inputChange(e) {
    this.setData({
      roomNumber: e.detail.value
    });
  },
  // 删除照片
  deleteImg(e) {
    let index = e.currentTarget.dataset.i;
    let imageList = this.data.imageList;
    imageList.splice(index, 1);
    this.setData({
      imageList
    });
  },
  // 提交
  submit() {
    const {
      active,
      describe,
      imageList,
      roomNumber
    } = this.data;
    if (active == 0) {
      wx.showToast({
        title: '请选择投诉类型',
        icon: 'none'
      });
      return;
    }
    if (describe == '') {
      wx.showToast({
        title: '请输入反馈内容',
        icon: 'none'
      });
      return;
    }
    if (roomNumber == '') {
      wx.showToast({
        title: '请输入房间号',
        icon: 'none'
      });
      return;
    }
    let param = {
      type: parseInt(active),
      desc: describe,
      room: roomNumber,
      imgPaths: JSON.stringify(imageList)
    };
    param.deviceSn = this.data.devInfo.deviceSn;
    param.placeId = this.data.devInfo.placeId;
    param.placeName = this.data.devInfo.placeName;
    console.log(param);
    API.complaintAdviceSubmit(param).then(res => {
      wx.showToast({
        title: '提交成功',
        icon: 'none'
      });
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/serviceHome/serviceHome'
        });
      }, 1500);
    });
  }
});