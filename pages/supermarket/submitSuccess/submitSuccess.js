import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
var app = getApp();
WXPage({
  data: {
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    tradeNo: "",
    showAd: 1
  },
  onLoad(options) {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
    this.setData({
      tradeNo: options.tradeNo,
      showAd: wx.getStorageSync('showAd')
    });
  },
  navOrder() {
    wx.navigateTo({
      url: `/pages/supermarket/workOrder/workOrder?tradeNo=${this.data.tradeNo}`
    });
  },
  backHome() {
    wx.reLaunch({
      url: '/pages/serviceHome/serviceHome'
    });
  }
});