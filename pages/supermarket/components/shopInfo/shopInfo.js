import WXComponent from "../../../../subwe/component";
const app = getApp();
WXComponent({
  properties: {
    goodsList: {
      type: Array,
      value: []
    },
    host: {
      type: String,
      value: ""
    },
    totalPrice: {
      type: Number,
      value: 0
    }
  },
  data: {}
});