import WXComponent from "../../../../subwe/component";
import { Index } from '../../../../apis/index';
const app = getApp();
const API = new Index();
WXComponent({
  properties: {
    number: {
      type: Number,
      value: 1
    },
    maxNum: {
      type: Number,
      value: 99999
    },
    goodsId: {
      type: Number
    }
  },
  data: {
    imageUrl: `${app.globalData.imageUrl}/zhongman2`
  },
  methods: {
    // 减
    reduceClick() {
      if (this.data.number === 0) return;
      this.updateCartNumber('plus', this.data.number - 1);
    },
    // 加
    plusClick() {
      this.updateCartNumber('reduce', this.data.number + 1);
    },
    updateCartNumber(name, number) {
      let {
        goodsId
      } = this.data;
      API.updateCart({
        goodsId,
        buyNum: number
      }).then(res => {
        name === "plus" ? --this.data.number : ++this.data.number;
        this.setData({
          number: this.data.number
        });
        this.triggerEvent("updateCartInfo");
      });
    }
  }
});