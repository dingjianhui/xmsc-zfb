import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    shopList: [{}, {}, {}],
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    tradeNo: "",
    goodsInfo: {},
    statusList: [{
      title: "待支付",
      id: 1,
      color: "red"
    }, {
      title: "待配送",
      id: 2,
      color: "green"
    }, {
      title: "待配送",
      id: 3,
      color: "yellow"
    }, {
      title: "已配送",
      id: 4,
      color: "gray"
    }, {
      title: "已取消",
      id: 5,
      color: "gray"
    }, {
      title: "已撤销",
      id: 6,
      color: "gray"
    }],
    host: ""
  },
  onLoad(options) {
    const tradeNo = options.tradeNo;
    this.setData({
      tradeNo
    });
    tradeNo && this.getGoodsDetailInfo();
  },
  // 获取商品详情信息
  getGoodsDetailInfo() {
    API.getGoodsDetails({
      tradeNo: this.data.tradeNo
    }).then(res => {
      const {
        data,
        host
      } = res.result;
      const arr = data.goodsOrderList.map(i => {
        return i.amount / 100;
      });
      data.totalPrice = arr.reduce((prev, curr, idx, arr) => {
        return prev + curr;
      });
      data.totalPrice = data.totalPrice.toFixed(2);
      this.setData({
        goodsInfo: data,
        host
      });
    });
  }
});