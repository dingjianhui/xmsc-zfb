import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
// pages/scendPages/serve/clean/clean.js

var util = require('../../../../utils/util');
var app = getApp();
import { Index } from '../../../../apis/index.js';
const API = new Index();
//引入datePicker的实例对象
import { DatePicker } from '../../../../utils/datePicker';
const params = {
  dateArr: ['今天', '明天', '后天']
};
//实例化对象
const datePicker = new DatePicker(params);
//制造日期的数组数据
const dateArr = datePicker.datePicker();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    // 时间
    dateArray: null,
    //picker-rang的值
    dateIndex: null,
    //picker-value的值
    dateVal: null,
    //显示的时间

    pageType: '客房清扫',
    notificationType: 1,
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    showAd: 1
  },
  onLoad: function (e) {
    // my.setNavigationBar({ title: this.data.pageType })

    // 加载时获取日期的组件
    this.set_date();
    //获取首页数据
    var info = JSON.parse(e.info);
    this.setData({
      info: info,
      showAd: wx.getStorageSync('showAd')
    });
  },
  //监听点击日期组件的事件变化
  datePickerChange(e) {
    let dateIndex = e.detail.value;
    this.setData({
      dateIndex,
      dateVal: datePicker.toDate(dateArr.dateAll, dateIndex),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateIndex)
    });
  },
  //赋值
  set_date() {
    this.setData({
      dateArray: dateArr.dateAll,
      dateIndex: dateArr.currentDateArr,
      dateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr)
    });
  },
  datePickerYMDHMS() {
    my.datePicker({
      format: 'yyyy-MM-dd HH:mm:ss',
      success: (res) => {
        const datetime = res.date.slice(0,16)
        this.setData({ dateVal: datetime, mydateVal: datetime })
      },
    });
  },
  // 输入备注
  getBeizhu: function (event) {
    var text = event.detail.value; //获取属于框输入的文字
    this.setData({
      beizhu: text
    });
  },
  formSubmit: function (e) {
    // 通过but点击事件触发后面的函数
    var that = this;
    // 提交表单
    var param = {
      "notificationType": this.data.notificationType,
      "placeId": this.data.info.placeId,
      "placeName": this.data.info.placeName,
      "deviceSn": this.data.info.deviceSn,
      "memberPhone": this.data.info.memberPhone,
      "placeRoom": this.data.info.adderss,
      "message": this.data.beizhu,
      "makeTime": this.data.mydateVal || util.formatTimeSerHome(new Date())
    };
    if (this.data.notificationType) {
      API.cleanFormSubmit(param).then(res => {}), wx.showToast({
        title: '提交成功！',
        icon: "none"
      }), setTimeout(function () {
        wx.redirectTo({
          url: '/pages/serviceHome/serviceHome'
        });
      }, 600);
    } else {
      wx.showToast({
        title: '请输入正确信息',
        duration: 2000
      });
    }
  }
});
