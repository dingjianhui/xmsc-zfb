import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
// pages/scendPages/serve/invoice/invoice.js

var util = require('../../../../utils/util');
var app = getApp();
import { Index } from '../../../../apis/index.js';
const API = new Index();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    pageType: '预约发票',
    notificationType: 5,
    invoiceType: 1,
    buttons: [{
      id: 1,
      name: '企业单位'
    }, {
      id: 2,
      name: '个人/非企业单位'
    }],
    form: {
      name: ''
    },
    showAd: 1
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    // wx.getStorageSync('login_key');
    // 按钮默认企业
    this.chooseMonthBtnClick();
    // 单选
    this.data.buttons[0].checked = true;
    //获取首页数据
    var info = JSON.parse(e.info);
    this.setData({
      buttons: this.data.buttons,
      info: info,
      showAd: wx.getStorageSync('showAd')
    });
  },
  // 切换表格
  chooseMonthBtnClick: function (e) {
    var invoiceType = this.data.invoiceType = 1;
    this.setData({
      changeColor1: true,
      changeColor2: false,
      chooseMonth: true,
      chooseYear: false,
      invoiceType: invoiceType
    });
  },
  chooseYearBtnClick: function (e) {
    var invoiceType = 2;
    this.setData({
      changeColor2: true,
      changeColor1: false,
      chooseMonth: false,
      chooseYear: true,
      invoiceType: invoiceType
    });
  },
  // 备注
  getBeizhu: function (event) {
    var text = event.detail.value; //获取属于框输入的文字
    this.setData({
      beizhu: text
    });
  },
  // 表单提交
  formSubmit: function (e) {
    var serviceInfo = [];
    if (this.data.invoiceType == 1) {
      serviceInfo = [e.detail.value.name, e.detail.value.shuihao, e.detail.value.jine, e.detail.value.shouji];
    } else {
      serviceInfo = [e.detail.value.name, e.detail.value.jine, e.detail.value.shouji];
    }

    // var myInvoice = [this.data.invoiceType];
    // serviceInfo.push(myInvoice);

    var param = {
      "notificationType": this.data.notificationType,
      "placeId": this.data.info.placeId,
      "deviceSn": this.data.info.deviceSn,
      "memberPhone": this.data.info.memberPhone,
      "placeRoom": this.data.info.adderss,
      "message": this.data.beizhu,
      "serviceInfo": serviceInfo,
      "invoiceType": this.data.invoiceType,
      "placeName": this.data.info.placeName
    };
    if (!e.detail.value.name) {
      wx.showToast({
        title: '名称为必填项',
        duration: 2000
      });
    } else if (this.data.invoiceType == 1 & !e.detail.value.shuihao) {
      wx.showToast({
        title: '税号为必填项',
        duration: 2000
      });
    } else if (!e.detail.value.jine) {
      wx.showToast({
        title: '金额为必填项',
        duration: 2000
      });
    } else if (!/^1[34578]\d{9}$/.test(e.detail.value.shouji)) {
      wx.showToast({
        title: '输入手机号不对',
        duration: 2000
      });
    } else if (1) {
      API.cleanFormSubmit(param).then(res => {}), wx.showToast({
        title: '提交成功！'
      });
      setTimeout(function () {
        wx.redirectTo({
          url: '/pages/serviceHome/serviceHome'
        });
      }, 600);
    }
  }
});