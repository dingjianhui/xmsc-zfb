import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
var util = require('../../../../utils/util');
var app = getApp();
import { Index } from '../../../../apis/index.js';
const API = new Index();
//引入datePicker的实例对象
import { DatePicker } from '../../../../utils/datePicker';
const params = {
  dateArr: ['今天', '明天', '后天']
};
//实例化对象
const datePicker = new DatePicker(params);
//制造日期的数组数据
const dateArr = datePicker.datePicker();
WXPage({
  data: {
    dateArray: null,
    //picker-rang的值
    dateIndex: null,
    //picker-value的值
    dateVal: null,
    //显示的时间
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    serviceList: [],
    defaultService: [{
      id: 1,
      name: '毛巾',
      number: 0
    }, {
      id: 2,
      name: '牙刷',
      number: 0
    }, {
      id: 3,
      name: '浴巾',
      number: 0
    }, {
      id: 4,
      name: '抽纸',
      number: 0
    }, {
      id: 5,
      name: '厕纸',
      number: 0
    }, {
      id: 6,
      name: '床被',
      number: 0
    }],
    baseInfo: {},
    showAd: 1
  },
  // 时间选择器
  //加载时获取日期的组件
  onLoad() {
    this.setData({
      baseInfo: wx.getStorageSync('baseInfo'),
      showAd: wx.getStorageSync("showAd")
    });
    this.set_date();
    this.getServiceListInfo();
  },
  //监听点击日期组件的事件变化
  datePickerChange(e) {
    let dateIndex = e.detail.value;
    this.setData({
      dateIndex,
      dateVal: datePicker.toDate(dateArr.dateAll, dateIndex),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateIndex)
    });
  },
  //赋值
  set_date() {
    this.setData({
      dateArray: dateArr.dateAll,
      dateIndex: dateArr.currentDateArr,
      dateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr)
    });
  },
  datePickerYMDHMS() {
    my.datePicker({
      format: 'yyyy-MM-dd HH:mm:ss',
      success: (res) => {
        const datetime = res.date.slice(0,16)
        this.setData({ dateVal: datetime, mydateVal: datetime })
      },
    });
  },
  getServiceListInfo() {
    const {
      baseInfo
    } = this.data;
    API.getServiceList({
      placeId: baseInfo.placeId,
      type: 1
    }).then(res => {
      const data = res.result;
      let list = data.length > 0 ? data : this.data.defaultService;
      list = list.map(i => ({
        ...i,
        number: 0
      }));
      this.setData({
        serviceList: list
      });
    });
  },
  getBeizhu(event) {
    this.setData({
      beizhu: event.detail.value
    });
  },
  navShop() {
    wx.navigateTo({
      url: '/pages/supermarket/supermarket/supermarket'
    });
  },
  reduceClick(e) {
    const {
      serviceList
    } = this.data;
    const index = Number(e.target.dataset.index);
    if (serviceList[index].number === 0) return;
    serviceList[index].number--;
    this.setData({
      serviceList: serviceList
    });
  },
  plusClick(e) {
    const {
      serviceList
    } = this.data;
    const index = Number(e.target.dataset.index);
    serviceList[index].number++;
    this.setData({
      serviceList
    });
  },
  // 表单提交
  formSubmit() {
    let {
      serviceList,
      baseInfo,
      beizhu,
      mydateVal
    } = this.data;
    serviceList = serviceList.filter(i => i.number > 0);
    if (!serviceList.length) {
      wx.showToast({
        title: '请选择要更换的日用品',
        duration: 2000,
        icon: "none"
      });
      return;
    }
    const list = serviceList.map(i => {
      return `${i.name}-${i.number}件`;
    });
    const login_key = wx.getStorageSync("login_key");
    const param = {
      notificationType: 2,
      placeName: baseInfo.placeName,
      serviceInfo: list,
      placeId: baseInfo.placeId,
      deviceSn: baseInfo.deviceSn,
      memberPhone: login_key.phone,
      placeRoom: baseInfo.room,
      message: beizhu,
      makeTime: mydateVal
    };
    API.cleanFormSubmit(param).then(res => {
      wx.showToast({
        title: res.message,
        icon: "none"
      }), setTimeout(() => {
        wx.navigateBack({
          delta: 1
        });
      }, 2000);
    });
  }
});
