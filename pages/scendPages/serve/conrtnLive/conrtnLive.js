import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
var util = require('../../../../utils/util');
var app = getApp();
import { Index } from '../../../../apis/index.js';
const API = new Index();
WXPage({
  data: {
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    baseInfo: {},
    roomInfo: {},
    roomPrice: 0,
    sourceRoomPrice: 0,
    // 原支付价格
    discountsAmount: 0,
    // 优惠金额
    number: 1,
    totalPrice: 0,
    memberName: "",
    memberPhone: "",
    statusBar: app.globalData.StatusBar,
    customBar: app.globalData.CustomBar,
    notConfigured: false
  },
  onLoad() {
    this.setData({
      baseInfo: wx.getStorageSync('baseInfo')
    });
    this.getRoomInfo();
  },
  getRoomInfo() {
    const {
      placeId,
      deviceSn,
      room
    } = this.data.baseInfo;
    API.getRoomTypeInfo({
      placeId,
      deviceSn,
      room
    }).then(res => {
      const {
        data
      } = res.result;
      if (data.id === 0) {
        this.setData({
          notConfigured: true
        });
        return;
      }
      data.discountsAmount = (data.discountsAmount / 100).toFixed(2);
      this.setData({
        roomInfo: data,
        roomPrice: (data.price / 100).toFixed(2),
        sourceRoomPrice: (data.price / 100).toFixed(2),
        discountsAmount: data.discountsAmount,
        totalPrice: (data.price / 100 - data.discountsAmount).toFixed(2)
      });
    });
  },
  backHome() {
    wx.navigateBack({
      delta: 1
    });
  },
  reduceClick() {
    let {
      number,
      roomPrice,
      roomInfo
    } = this.data;
    if (number <= 1) return;
    this.setData({
      number: --number,
      sourceRoomPrice: (roomPrice * number).toFixed(2),
      discountsAmount: (roomInfo.discountsAmount * number).toFixed(2),
      totalPrice: (roomPrice * number - roomInfo.discountsAmount * number).toFixed(2)
    });
  },
  plusClick() {
    let {
      number,
      roomPrice,
      roomInfo
    } = this.data;
    if (number === 999) return;
    this.setData({
      number: ++number,
      sourceRoomPrice: (roomPrice * number).toFixed(2),
      discountsAmount: (roomInfo.discountsAmount * number).toFixed(2),
      totalPrice: (roomPrice * number - roomInfo.discountsAmount * number).toFixed(2)
    });
  },
  nameInput(e) {
    this.setData({
      memberName: e.detail.value
    });
  },
  phoneInput(e) {
    this.setData({
      memberPhone: e.detail.value
    });
  },
  getTimeInfo(time) {
    let date = new Date(time);
    const year = date.getFullYear();
    let month = util.addZero(date.getMonth() + 1);
    // month = month < 10 ? `0${month}` : month
    let day = util.addZero(date.getDate());
    // day = day < 10 ? `0${day}` : day
    const hour = util.addZero(date.getHours());
    const minute = util.addZero(date.getMinutes());
    const second = util.addZero(date.getSeconds());
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  },
  async confirmPay() {
    const {
      baseInfo,
      roomInfo,
      memberName,
      memberPhone,
      number
    } = this.data;
    const openid = wx.getStorageSync("login_key").openid;
    if (!memberName) {
      wx.showToast({
        title: '请输入您的姓名',
        icon: 'none'
      });
      return;
    }
    if (!memberPhone) {
      wx.showToast({
        title: '请输入联系电话',
        icon: 'none'
      });
      return;
    }
    const date = new Date().getTime();
    const inTime = this.getTimeInfo(date);
    const leaveTime = this.getTimeInfo(date + number * 1000 * 60 * 60 * 24);
    console.log(inTime, leaveTime);
    API.createRoomOrder({
      deviceSn: baseInfo.deviceSn,
      openid,
      roomTypeId: roomInfo.id,
      roomId: roomInfo.roomId,
      inTime,
      leaveTime,
      memberName,
      memberPhone
    }).then(res => {
      this.wxPayment(res.result);
    });
  },
  async confirmBelowLinePay() {
    const {
      baseInfo,
      roomInfo,
      memberName,
      memberPhone,
      number
    } = this.data;
    const openid = wx.getStorageSync("login_key").openid;
    if (!memberName) {
      wx.showToast({
        title: '请输入您的姓名',
        icon: 'none'
      });
      return;
    }
    if (!memberPhone) {
      wx.showToast({
        title: '请输入联系电话',
        icon: 'none'
      });
      return;
    }
    const date = new Date().getTime();
    const inTime = this.getTimeInfo(date);
    const leaveTime = this.getTimeInfo(date + number * 1000 * 60 * 60 * 24);
    console.log(inTime, leaveTime);
    API.createBelowLineRoomOrder({
      deviceSn: baseInfo.deviceSn,
      openid,
      roomTypeId: roomInfo.id,
      roomId: roomInfo.roomId,
      inTime,
      leaveTime,
      memberName,
      memberPhone
    }).then(res => {
      wx.reLaunch({
        url: `/pages/serviceMy/workList/workList`
      });
    });
  },
  wxPayment(data) {

    let { out_trade_no, trade_no } = data.AliPayTradeCreateResBody || {}
    my.tradePay({
      tradeNO: out_trade_no,
      success: (res) => {
        wx.hideLoading();
        console.log("wxPayment-success", res);
        if(Number(res.resultCode) === 9000) {
          wx.showToast({title: '支付成功', icon: 'none'})
          setTimeout(() => {wx.reLaunch({url: `/pages/serviceMy/workList/workList`})}, 1000)
          return
        }
        wx.showToast({title: res.memo || '支付失败', icon: 'none'})
      },
      fail: (res) => {
        wx.hideLoading()
        console.log("fail info", res);
        wx.showToast({
          title: '支付中断',
          icon: 'none'
        });
      }
    });

    // const param = JSON.parse(data);
    // wx.requestPayment({
    //   timeStamp: param.timeStamp,
    //   nonceStr: param.nonceStr,
    //   package: param.package,
    //   signType: param.signType,
    //   paySign: param.paySign,
    //   success() {
    //     setTimeout(() => {
    //       wx.reLaunch({
    //         url: `/pages/serviceMy/workList/workList`
    //       });
    //     }, 1000);
    //   },
    //   fail(res) {
    //     console.log("fail info", res);
    //   }
    // });
  }
});
