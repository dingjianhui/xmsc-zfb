import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
var util = require('../../../../utils/util');
var app = getApp();
import { Index } from '../../../../apis/index.js';
const API = new Index();
//引入datePicker的实例对象
import { DatePicker } from '../../../../utils/datePicker';
const params = {
  dateArr: ['今天', '明天', '后天']
};
const datePicker = new DatePicker(params);
//制造日期的数组数据
const dateArr = datePicker.datePicker();
WXPage({
  data: {
    // 时间
    dateArray: null,
    //picker-rang的值
    dateIndex: null,
    //picker-value的值
    dateVal: null,
    //显示的时间
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    serviceList: [],
    baseInfo: {},
    defaultList: [{
      id: 1,
      name: '电视'
    }, {
      id: 2,
      name: '空调'
    }, {
      id: 3,
      name: '淋浴'
    }, {
      id: 4,
      name: '马桶'
    }, {
      id: 5,
      name: '桌椅'
    }, {
      id: 6,
      name: '水壶'
    }, {
      id: 7,
      name: '门窗'
    }, {
      id: 8,
      name: '床铺'
    }, {
      id: 9,
      name: '灯具'
    }, {
      id: 10,
      name: '沙发'
    }, {
      id: 11,
      name: '地板'
    }, {
      id: 12,
      name: '其他'
    }],
    showAd: 1
  },
  onLoad() {
    this.setData({
      baseInfo: wx.getStorageSync('baseInfo'),
      showAd: wx.getStorageSync('showAd')
    });
    this.set_date();
    this.getServiceListInfo();
  },
  //监听点击日期组件的事件变化
  datePickerChange(e) {
    let dateIndex = e.detail.value;
    this.setData({
      dateIndex,
      dateVal: datePicker.toDate(dateArr.dateAll, dateIndex),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateIndex)
    });
  },
  getServiceListInfo() {
    const {
      baseInfo
    } = this.data;
    API.getServiceList({
      placeId: baseInfo.placeId,
      type: 2
    }).then(res => {
      const {
        defaultList
      } = this.data;
      const data = res.result;
      let list = data.length ? data : defaultList;
      this.setData({
        serviceList: list
      });
    });
  },
  switchChange(e) {
    const info = e.target.dataset;
    let list = this.data.serviceList;
    list[info.index].checked = info.checked;
    this.setData({
      serviceList: list
    });
  },
  //赋值
  set_date() {
    this.setData({
      dateArray: dateArr.dateAll,
      dateIndex: dateArr.currentDateArr,
      dateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr),
      mydateVal: datePicker.tomyDate(dateArr.dateAll, dateArr.currentDateArr)
    });
  },
  datePickerYMDHMS() {
    my.datePicker({
      format: 'yyyy-MM-dd HH:mm:ss',
      success: (res) => {
        const datetime = res.date.slice(0,16)
        this.setData({ dateVal: datetime, mydateVal: datetime })
      },
    });
  },
  getBeizhu(event) {
    this.setData({
      beizhu: event.detail.value
    });
  },
  // 表单提交
  mysubmit() {
    let {
      serviceList,
      baseInfo,
      beizhu,
      mydateVal
    } = this.data;
    serviceList = serviceList.filter(i => i.checked);
    if (!serviceList.length) {
      wx.showToast({
        title: '请选择维修位置',
        duration: 2000,
        icon: "none"
      });
      return;
    }
    const list = serviceList.map(i => {
      return i.name;
    });
    const phone = wx.getStorageSync("login_key").phone;
    const param = {
      notificationType: 3,
      serviceInfo: list,
      placeId: baseInfo.placeId,
      deviceSn: baseInfo.deviceSn,
      memberPhone: phone,
      placeRoom: baseInfo.room,
      placeName: baseInfo.placeName,
      message: beizhu,
      makeTime: mydateVal || util.formatTimeSerHome(new Date())
    };
    API.cleanFormSubmit(param).then(res => {
      wx.showToast({
        title: res.message,
        icon: "none"
      }), setTimeout(() => {
        wx.navigateBack({
          delta: 1
        });
      }, 2000);
    });
  }
});
