import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { Index } from '../../apis/index.js';
const app = getApp();
const API = new Index();
const {
  formatSeconds,
  PHONE_NUMBER
} = require("../../utils/util");
WXPage({
  data: {
    deviceSn: '',
    tradeNo: '',
    loginInfo: '',
    showAd: 1,
    isCtrip: false,
    PHONE_NUMBER,
    imageUrl: `${app.globalData.imageUrl}`
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.setData({
      loginInfo: wx.getStorageSync('login_key'),
      tradeNo: options.tradeno,
      deviceSn: options.deviceSn,
      showAd: wx.getStorageSync("showAd"),
      isCtrip: app.globalData.isCtrip
    });
    // 获取设备信息-携程跳转需要
    API.getDeviceInfo({
      "deviceSn": this.data.deviceSn
    }).then(res => {
      this.setData({
        devInfo: res.result
      });
    });
  },
  onShow: function () {
    // 在页面中定义插屏广告
    let interstitialAd = null;

    // 在页面onLoad回调事件中创建插屏广告实例
    // if (wx.createInterstitialAd) {
    //   interstitialAd = wx.createInterstitialAd({
    //     adUnitId: 'adunit-05279f7b7ae32bcf'
    //   });
    //   interstitialAd.onLoad(() => {});
    //   interstitialAd.onError(err => {});
    //   interstitialAd.onClose(() => {});
    // }

    // 在适合的场景显示插屏广告
    if (interstitialAd) {
      interstitialAd.show().catch(err => {
        console.error(err);
      });
    }
  },
  // 跳转携程
  toCtrip: function () {
    // wx.uma.trackEvent('OrderCtripBanner');
    // wx.reportEvent("order_complete_ctrip", {});
    wx.navigateToMiniProgram({
      appId: 'wx0e6ed4f51db9d078',
      path: 'pages/hotelplanning/aggregate/wifiaggregate?a=' + this.data.devInfo.hotelId
    });
  },
  toAdFuli: function () {
    // wx.reportEvent("fuli_ad", {});
    wx.navigateToMiniProgram({
      appId: 'wxe9b6c1ec4c05536b',
      path: '/packageG/newDiy/newDiy?page_id=4'
    });
  },
  // 跳转至订单详情
  toOrderList: function () {
    wx.navigateTo({
      url: `/pages/mine/orderdetail/orderdetail?tradeNo=${this.data.tradeNo}&deviceSn=${this.data.deviceSn}`
    });
  },
  // 跳转飞猪小程序
  toFeizhu: function () {
    // wx.reportEvent("feizhu_banner", {});
    wx.navigateToMiniProgram({
      appId: 'wx6a96c49f29850eb5',
      path: 'pages/hotel-search/searchlist/index?fpid=17016&ttid=12wechat000004650'
    });
  },
  onback: function () {
    wx.navigateTo({
      url: '/pages/serviceMy/serviceMy'
    });
  },
  // 足浴
  thirdPageGotoLavipeditum() {
    // wx.reportEvent("service_meituan_sanazuyu", {});
    if (this.data.devInfo.city === '武汉市') {
      wx.navigateTo({
        url: '/pages/webview/wyWebview/wyWebview'
      });
    } else if (this.data.devInfo.city === '衡阳市') {
      wx.navigateTo({
        url: '/pages/webview/eyWebview/eyWebview'
      });
    } else if (this.data.devInfo.city === '南宁市') {
      wx.navigateTo({
        url: '/pages/webview/xmNnWebview/xmNnWebview'
      });
    } else {
      wx.showToast({
        title: "目前该城市暂不支持",
        icon: "none"
      });
      return;
    }
  }
});
