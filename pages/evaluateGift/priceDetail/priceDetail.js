import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
const app = getApp();
import { Index } from '../../../apis/index.js';
const API = new Index();
WXPage({
  data: {
    goodsDetails: {},
    goodsType: ['线充优惠券', '实物商品', '现金红包'],
    host: wx.getStorageSync("host")
  },
  onLoad(options) {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
    this.getGoodsDetails(options.id);
  },
  // 获取商品信息
  getGoodsDetails(id) {
    if (id > 0) {
      API.getPraiseDetail({
        id: Number(id)
      }).then(res => {
        const data = res.result;
        if (data.imgList != "") {
          data.imgList = JSON.parse(data.imgList);
        }
        this.setData({
          goodsDetails: data
        });
      });
    } else {
      wx.showToast({
        title: '加载失败',
        duration: 2000,
        icon: "none"
      });
    }
  },
  navigateFree: function (e) {
    wx.navigateBack({
      delta: 1
    });
  }
});