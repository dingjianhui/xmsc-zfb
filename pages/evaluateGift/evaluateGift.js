import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
const app = getApp();
import { Index } from '../../apis/index';
import util from '../../utils/util';
const API = new Index();
WXPage({
  data: {
    pageNum: 1,
    pageSize: 10,
    dataList: [],
    showMoreVisable: false,
    imgUrl: '',
    defaultImg: '/static/zhongman2/img_Coupon@2x.png',
    host: wx.getStorageSync("host"),
    loginInfo: {}
  },
  onLoad(ev) {
    console.log('onShow', ev);
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
    this.setData({
      loginInfo: wx.getStorageSync('login_key')
    });
    this.setData({
      imgUrl: util.IMG_URL
    });
    if (ev.deviceSn) {
      app.updateDev(ev.deviceSn);
      this.getLoginToken();
    } else this.getGoodsList(this.data.pageNum);
  },
  onShow(ev) {
    console.log('e--onShow', ev);
  },
  // 登录
  getLoginToken(value) {
    API.getLoginToken().then(res => {
      if (!res) return;
      let loginInfo = res.result;
      this.setData({
        loginInfo: loginInfo
      });
      wx.setStorageSync("login_key", loginInfo);
      wx.setStorageSync("host", res.result.host);
      this.setData({
        host: res.result.host
      });
      this.getGoodsList(this.data.pageNum);
    });
  },
  // 服务评价提醒授权
  messageEvaluateAlertAuthorization() {
    console.log('messageEvaluateAlertAuthorization');
    let tmplIds = ['a3Iv2WMm497jJETnzpSvET3gwAomvj01xHRhSDoW3S8'];
    let that = this;
    // wx.requestSubscribeMessage({
    //   tmplIds,
    //   success(res) {
    //     let params = {
    //       deviceSn: app.globalData.deviceSn,
    //       openid: that.data.loginInfo.openid
    //     };
    //     let acceptTemIds = [];
    //     for (let key in res) if (key !== 'errMsg') if (res[key] === 'accept') {
    //       acceptTemIds.push(key);
    //       params.templateId = key;
    //       API.fixedRemindRss(params).then(res => {
    //         console.log(res);
    //       });
    //     }
    //     params.templateId = acceptTemIds;
    //     // if (acceptTemIds.length === 0) return
    //     // console.log(res)
    //     // wx.showModal({
    //     //   title: '提示',
    //     //   content: (countdownSecond / 3600 ) + '小时后，归还通知提醒',
    //     //   showCancel: false,
    //     //   success (res) {
    //     //     if (res.confirm) {
    //     //       console.log('用户点击确定')
    //     //     } else if (res.cancel) {
    //     //       console.log('用户点击取消')
    //     //     }
    //     //   }
    //     // })
    //   },
    //
    //   fail(err) {
    //     console.log("requestSubscribeMessage-err", err);
    //   }
    // });
  },
  getGoodsList: function (pageNum) {
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    var param = {
      pageNo: pageNum,
      pageSize: this.data.pageSize
    };
    if (param.pageNo === 1) this.data.dataList = [];
    API.getGoodsList(param).then(res => {
      wx.hideLoading();
      var dataList = this.data.dataList.concat(res.result.data);
      var showMore = false;
      var stockList = [];
      // 如果库存为空则不展示
      for (var i = 0; i < dataList.length; i++) {
        if (dataList[i].num > 0) {
          stockList.push(dataList[i]);
        }
      }
      if (res.result.totalCount > dataList.length) {
        showMore = true;
      }
      this.setData({
        index: i,
        showMoreVisable: showMore,
        stockList: stockList
      });
    });
  },
  // 跳转详情
  goodsDeatail(e) {
    e.mark =  e.target.dataset
    if (e.mark.id === 0) wx.navigateTo({
      url: '/pages/evaluateGift/drawInput/index?id=' + e.mark.id
    });else wx.navigateTo({
      url: '/pages/evaluateGift/myPrice/myPrice?id=' + e.mark.id
    });
  },
  // 显示更多
  showMore: function () {
    var pageNum = this.data.pageNum += 1;
    this.getGoodsList(pageNum);
  },
  toKeFu: function () {
    // wx.reportEvent("event_hp_kf", {});
    wx.navigateTo({
      url: '/pages/mine/zmKf/zmkf'
    });
  }
});
