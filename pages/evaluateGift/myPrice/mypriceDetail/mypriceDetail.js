import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
const app = getApp();
import { Index } from '../../../../apis/index';
const API = new Index();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    region: ['', '', ''],
    defaultFont: '请选择所在地区（省/市/区县）',
    id: 1,
    regionCode: [],
    regionValue: [],
    addressDetails: {}
  },
  setInputData(e) {
    let key = e.target.dataset.key
    this.setData({ [key]: e.detail.value })
  },
  onLoad(options) {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
    this.getAddressDetails(options.id);
  },
  //  省市区控件
  bindRegionChange: function () {
    my.regionPicker({
      success: res => {
        console.log('my.regionPicker', res)
        this.setData({
          region: res.data,
          regionCode: res.code,
          regionValue: res.data
        });
      }
    })
  },


  // 提交
  submit: function (e) {
    let id = this.data.id;
    let myname = this.data.myname;
    if (this.data.addressDetails.id > 0 && !myname) {
      myname = this.data.addressDetails.name;
    }
    let phone = this.data.phone;
    if (this.data.addressDetails.id > 0 && !/^1[34578]\d{9}$/.test(phone)) {
      phone = this.data.addressDetails.phone;
    }
    let zipCode = this.data.zipCode;
    if (this.data.addressDetails.id > 0 && !zipCode) {
      zipCode = this.data.addressDetails.postcode;
    }
    let region = this.data.region;
    if (this.data.addressDetails.id > 0 && !region) {
      var regionCode = [this.data.addressDetails.provinceCode, this.data.addressDetails.cityCode, this.data.addressDetails.countyCode];
      var regionValue = [this.data.addressDetails.provinceName, this.data.addressDetails.cityName, this.data.addressDetails.countyName];
      this.setData({
        regionCode: regionCode,
        regionValue: regionValue
      });
    }
    let addressDetail = this.data.addressDetail;
    if (this.data.addressDetails.id > 0 && !addressDetail) {
      addressDetail = this.data.addressDetails.address;
    }
    if (!myname) {
      wx.showToast({
        title: '请填写联系人姓名',
        icon: 'none'
      });
      return false;
    }
    if (!/^1[34578]\d{9}$/.test(phone)) {
      wx.showToast({
        title: '请填写正确手机',
        icon: 'none'
      });
      return false;
    }
    if (!addressDetail & id) {
      wx.showToast({
        title: '请填写正确地址',
        icon: 'none'
      });
      return false;
    }
    if (!region & id) {
      wx.showToast({
        title: '请选择所在地区',
        icon: 'none'
      });
      return false;
    }
    let param = {
      "name": myname,
      "phone": phone,
      "address": addressDetail,
      "postcode": zipCode,
      "provinceCode": this.data.regionCode[0],
      "cityCode": this.data.regionCode[1],
      "countyCode": this.data.regionCode[2],
      "provinceName": this.data.regionValue[0],
      "cityName": this.data.regionValue[1],
      "countyName": this.data.regionValue[2]
    };
    if (this.data.addressDetails.id > 0) {
      param["id"] = this.data.addressDetails.id;
      API.addressUpdate(param).then(res => {
        wx.showToast({
          title: '修改成功',
          duration: 2000
        });
        wx.redirectTo({
          url: '/pages/evaluateGift/myPrice/mypriceAddress/mypriceAddress'
        });
      });
    } else {
      API.addressCreate(param).then(res => {
        wx.showToast({
          title: '新增成功',
          duration: 2000
        });
        wx.redirectTo({
          url: '/pages/evaluateGift/myPrice/mypriceAddress/mypriceAddress'
        });
      });
    }
  },
  getAddressDetails: function (id) {
    API.getAddressDetails({
      id: Number(id)
    }).then(res => {
      const data = res.result;
      this.setData({
        addressDetails: data
      });
    });
  }
});
