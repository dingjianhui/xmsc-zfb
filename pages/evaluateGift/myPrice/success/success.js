import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
var app = getApp();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    imageUrl: `${app.globalData.imageUrl}/zhongman2`
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},
  toBack() {
    wx.navigateBack({
      delta: 2
    });
  },
  toWorkList() {
    wx.redirectTo({
      url: '/pages/serviceMy/workList/workList'
    });
  }
});