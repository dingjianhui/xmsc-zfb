import wx from '../../../../subwe/bridge';
import WXPage from "../../../../subwe/page";
const app = getApp();
import { Index } from '../../../../apis/index';
const API = new Index();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    dataList: [],
    imageUrl: `${app.globalData.imageUrl}/zhongman2`
  },
  onLoad() {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
  },
  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function () {
    this.getAddressList();
    this.getLoginToken();
  },
  getAddressList: function () {
    API.addressList().then(res => {
      var list = [];
      var dataList = res.result;
      dataList.forEach(function (item, index) {
        item.addressDetails = item.provinceName + " " + item.cityName + " " + item.countyName + " " + item.address;
        list.push(item);
      });
      this.setData({
        dataList: list
      });
    });
  },
  // 登录
  getLoginToken() {
    API.getLoginToken().then(res => {
      if (!res) return;
      let loginInfo = res.result;
      this.setData({
        loginInfo: loginInfo
      });
      wx.setStorageSync("login_key", loginInfo);
      wx.setStorageSync("host", res.result.host);
    });
  },
  // 跳转详情
  addressDeatail(e) {
    e.mark = e.target.dataset
    console.log("e===>", e);
    wx.redirectTo({
      url: '/pages/evaluateGift/myPrice/mypriceDetail/mypriceDetail?id=' + e.mark.id
    });
  },
  // 删除地址
  deleteAddress(e) {
    e.mark = e.target.dataset
    const _this = this;
    wx.showModal({
      title: '提示',
      content: '确认要删除这条地址吗?',
      success() {
        API.deleteAddress({
          id: e.mark.id
        }).then(res => {
          wx.showToast({
            title: '删除成功',
            icon: 'none'
          });
          _this.getAddressList();
        });
      }
    });
  },
  // 添加地址
  addAddress: function () {
    wx.redirectTo({
      url: '/pages/evaluateGift/myPrice/mypriceDetail/mypriceDetail'
    });
  },
  // 跳转回提交页面
  selectAddress: function (e) {
    e.mark = e.target.dataset
    // let pages = getCurrentPages();
    // let prevPage = pages[pages.length - 2];
    // prevPage.setData({ id: e.mark.id });
    wx.setStorageSync('addressId', e.mark.id)
    wx.navigateBack({delta: 1});
  }
});
