import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { Index } from '../../apis/index.js';
const app = getApp();
const API = new Index();
const {
  util
} = require("../../utils/util");
const {
  formatSeconds,
  channel,
  vuid_pre,
  place
} = require("../../utils/util");
WXPage({
  data: {
    useTime: '',
    settMoney: 0,
    returnMoney: 0,
    productId: '',
    ruleInfo: '',
    deviceSn: '',
    loginInfo: '',
    showAd: 1,
    isCtrip: false,
    devInfo: {},
    isshow: false,
    type: "interstitial",
    mediaId: "dev_android_xcx_video",
    adUrl: '',
    iconUrl: '',
    phone: ''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.setData({
      settMoney: options.settMoney,
      returnMoney: options.returnMoney,
      productId: options.productId,
      loginInfo: wx.getStorageSync('login_key'),
      deviceSn: options.deviceSn,
      showAd: wx.getStorageSync("showAd"),
      isCtrip: app.globalData.isCtrip
    });
    if (!this.data.loginInfo) {
      // 登录
      await API.getLoginToken().then(res => {
        this.setData({
          loginInfo: res.result
        });
        wx.setStorageSync("login_key", res.result);
      });
    }

    // 获取设备信息
    var deviceInfo = null;
    var param = {
      "deviceSn": this.data.deviceSn
    };
    await API.getDeviceInfo(param).then(res => {
      let phone = res.result.prodType !== 1 ? util.ZM_PHONE_NUMBER : util.PHONE_NUMBER;
      deviceInfo = res.result;
      this.setData({
        devInfo: deviceInfo,
        phone: phone
      });
    });
    console.log('订单完成 - 设备信息 - deviceInfo', deviceInfo);
    let use_time = '';
    if (options.useTime < 0) options.useTime = 0;
    if (options.useTime >= 60) {
      use_time = formatSeconds(options.useTime);
    } else {
      use_time = options.useTime + "秒";
    }
    console.log('订单完成-使用时长', use_time);
    var ruleInfo = '';
    deviceInfo.ruleInfo.map(item => {
      if (item.productId === this.data.productId) {
        if (deviceInfo.feeMode === 2) {
          // 套餐模式
          ruleInfo = '(套餐)' + item.payMoney / 100 + '元充电' + item.useDuration + '分钟';
        } else {
          // ruleInfo = item.payIntervalPrice/100 + '元/' + item.payInterval + '分钟，封顶' + item.maxPayPrice/100 + '元,最大使用时长' + item.maxTime + '分钟'
          // ruleInfo = '前' + item.baseTimes/60 + '小时(' + item.basePrice/100 + '元/小时)，' + item.baseTimes/60 + '小时后(' + item.payIntervalPrice/100 + '元/小时)，24小时封顶' + item.maxPayPrice/100 + '元'
          ruleInfo = '前' + item.baseTimes / 60 + '小时' + item.basePrice / 100 + '元，之后' + item.payIntervalPrice / 100 + '元/小时，24小时封顶' + item.maxPayPrice / 100 + '元，单笔封顶' + item.maxPayPrice / 100 + '元';
        }
      }
    });
    console.log('订单完成-ruleInfo', ruleInfo);
    this.setData({
      useTime: use_time,
      ruleInfo: ruleInfo
    });
  },
  toIndex: function () {
    wx.navigateTo({
      url: '/pages/padline/padline'
    });
  },
  toCtrip: function () {
    // wx.navigateTo({
    //   url: '/pages/mine/ctrip/ctrip',
    // })
    // wx.uma.trackEvent('OrderCtripBanner');
    wx.navigateToMiniProgram({
      appId: 'wx0e6ed4f51db9d078',
      path: 'pages/hotelplanning/aggregate/wifiaggregate?a=' + this.data.devInfo.hotelId
    });
  },
  toKaAd: function () {
    // wx.reportEvent("order_detail_ka_click", {});
    wx.navigateTo({
      url: '/pages/mine/adka/adka'
    });
  },
  toMyCenter: function () {
    wx.redirectTo({
      url: '/pages/mine/mine'
    });
  },
  closeYdAd: function () {
    this.setData({
      isshow: false
    });
  },
  onShow: function () {
    // 在页面中定义插屏广告
    let interstitialAd = null;

    // 在页面onLoad回调事件中创建插屏广告实例
    // if (wx.createInterstitialAd) {
    //   interstitialAd = wx.createInterstitialAd({
    //     adUnitId: 'adunit-05279f7b7ae32bcf'
    //   });
    //   interstitialAd.onLoad(() => {});
    //   interstitialAd.onError(err => {});
    //   interstitialAd.onClose(() => {});
    // }

    // 在适合的场景显示插屏广告
    if (interstitialAd) {
      interstitialAd.show().catch(err => {
        console.error(err);
      });
    }
  }
});
