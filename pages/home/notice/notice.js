import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    devInfo: {},
    noticeList: []
  },
  onLoad() {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFFFFF'
    });
    this.getDeviceInfo();
  },
  // 获取设备信息
  getDeviceInfo() {
    let param = {
      "deviceSn": app.globalData.deviceSn
    };
    API.getRoomDeviceInfo(param).then(res => {
      this.setData({
        devInfo: res.result
      });
      this.getNoticeList();
    });
  },
  getNoticeList() {
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    API.getNoticeList({
      pageNo: 1,
      pageSize: 20,
      placeId: this.data.devInfo.placeId
    }).then(res => {
      wx.hideLoading();
      this.setData({
        noticeList: res.result.data
      });
    });
  }
});