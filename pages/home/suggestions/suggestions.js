import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    problemList: [{
      title: '意见建议',
      key: 1
    }, {
      title: '服务问题',
      key: 2
    }, {
      title: '体验问题',
      key: 3
    }, {
      title: '住房问题',
      key: 4
    }, {
      title: "设备问题",
      key: 5
    }, {
      title: "其它问题",
      key: 9
    }],
    activeIndex: "",
    descLen: 0,
    uploadList: [],
    desc: "",
    baseInfo: {},
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    limit: 4,
    showAd: 1
  },
  onLoad() {
    this.setData({
      baseInfo: wx.getStorageSync('baseInfo'),
      showAd: wx.getStorageSync('showAd')
    });
  },
  tabChange(e) {
    this.setData({
      activeIndex: e.currentTarget.dataset.id
    });
  },
  bindTextarea(e) {
    const value = e.detail.value;
    const len = value.length;
    if (len > 100) return;
    this.setData({
      descLen: len,
      desc: value
    });
  },
  // deleteImg(e) {
  //   const index = Number(e.currentTarget.dataset.index)
  //   const list = this.data.uploadList.filter((i, key) => index !== key)
  //   this.setData({
  //     uploadList: list
  //   })
  // },
  submitClick() {
    const {
      baseInfo,
      uploadList,
      activeIndex,
      desc
    } = this.data;
    if (!activeIndex) {
      wx.showToast({
        title: '请选择问题类型',
        icon: 'none'
      });
      return;
    }
    if (!desc) {
      wx.showToast({
        title: '请输入问题描述或建议',
        icon: 'none'
      });
      return;
    }
    API.submitReport({
      placeId: baseInfo.placeId,
      deviceSn: baseInfo.deviceSn,
      imgPaths: JSON.stringify(uploadList),
      type: activeIndex,
      room: baseInfo.room,
      placeName: baseInfo.placeName,
      address: baseInfo.address,
      desc: desc
    }).then(res => {
      wx.showToast({
        title: '提交成功',
        icon: 'none'
      });
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/serviceHome/serviceHome'
        });
      }, 1500);
    });
  },
  updateImg(e) {
    this.setData({
      uploadList: e.detail
    });
  }
});