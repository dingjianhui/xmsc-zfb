import WXPage from "../../../subwe/page";
// pages/mine/help/help.js
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    TabCur: 0,
    show: true,
    dataList: [
      {
        title: '密码线设备故障/无法充电?',
        content: '密码不是以短信方式发送的，请查看是否有进入密码页。若未出现密码页，请二次扫码检查是否能进入密码页，如果依旧无法进入，请先进行故障申报，或者联系我们客服为您解决问题',
        show: true
      },
      {
        title: '如何结束订单?',
        content: '①、计时模式的充电线使用完毕后需要重新扫码进入小程序结束订单；②、套餐模式的充电线使用完成后自动归还。',
        show: true
      },
      {
        title: '怎么看之前的密码?',
        content: '重新扫码进入小曼速充小程序，进入订单就能看到密码了，也可以点右上角重新获取密码。',
        show: true
      },
      {
        title: '断电后如何继续充电?',
        content: '设备保持通电，重新扫码点击获取密码，输入新密码进行充电。',
        show: true
      },
    ]
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id
    });
  },
  // 展开内容区域
  showContent(e) {
    let index = e.currentTarget.dataset.i;
    let dataList = this.data.dataList;
    dataList[index].show = !dataList[index].show;
    this.setData({
      dataList
    });
  }
});