import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
const app = getApp();
const util = require("../../utils/util");
const {
  getQueryString,
  COMPANY_NAME
} = require("../../utils/util");
const {
  baseUrl,
  yundianUrl
} = require("../../config");
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    iconList: [{
      icon: 'cardboardfill',
      color: 'red',
      badge: 120,
      name: '我的订单'
    }, {
      icon: 'recordfill',
      color: 'orange',
      badge: 1,
      name: '使用帮助'
    }, {
      icon: 'picfill',
      color: 'yellow',
      badge: 0,
      name: '扫码归还'
    }, {
      icon: 'noticefill',
      color: 'olive',
      badge: 22,
      name: '附近网点'
    }, {
      icon: 'upstagefill',
      color: 'cyan',
      badge: 0,
      name: '故障申报'
    }, {
      icon: 'clothesfill',
      color: 'blue',
      badge: 0,
      name: '我要加盟'
    }, {
      icon: 'discoverfill',
      color: 'purple',
      badge: 0,
      name: '意见反馈'
    }, {
      icon: 'questionfill',
      color: 'mauve',
      badge: 0,
      name: '关于我们'
    }],
    gridCol: 3,
    skin: true,
    balanceStr: "",
    COMPANY_NAME,
    showAd: 1,
    phone: '',
    devInfo: {}
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    var that = this;
    that.setData({
      balanceStr: "0.00",
      showAd: wx.getStorageSync("showAd"),
      loginInfo: wx.getStorageSync('login_key')
    });
    if (!that.data.loginInfo) {
      // 登录
      await API.getLoginToken().then(res => {
        that.setData({
          loginInfo: res.result
        });
        wx.setStorageSync("login_key", res.result);
      });
    }
    if (app.globalData.deviceSn) {
      // 同步获取设备信息
      await API.getDeviceInfo({
        "deviceSn": app.globalData.deviceSn
      }).then(res => {
        let phone = res.result.prodType !== 1 ? util.ZM_PHONE_NUMBER : util.PHONE_NUMBER;
        that.setData({
          devInfo: res.result,
          showAd: res.result.isAd,
          phone: phone
        });
      });
    } else {
      that.setData({
        phone: util.PHONE_NUMBER
      });
    }
  },
  onItemTap: function () {
    wx.makePhoneCall({
      // phoneNumber: '18680686420',
    });
  },
  onScan() {
    var that = this;
    wx.scanCode({
      success: res => {
        var result = res.result;
        let q = decodeURIComponent(result);
        app.globalData.deviceSn = getQueryString(q, "dev");
        let pcl = getQueryString(q, "pcl");
        if (q.includes('yundian') || pcl === '10041') {
          app.globalData.apiUrl = yundianUrl ? yundianUrl : 'https://gmzfb.cnman.cn/v1/wx/';
          app.globalData.isZm = false;
        } else if (q.includes('blue.chinaman.ink')) {
          // 二代线
          app.globalData.apiUrl = baseUrl ? baseUrl : 'https://sczfb.cnman.cn/v1/wx/';
          app.globalData.isBlue = true;
        } else {
          app.globalData.apiUrl = baseUrl ? baseUrl : 'https://sczfb.cnman.cn/v1/wx/';
        }
        wx.navigateTo({
          url: '/pages/padline/padline'
        });
      }
    });
  },
  concatService() {
    wx.makePhoneCall({
      phoneNumber: util.PHONE_NUMBER
    });
  }

  // tobaiduAD: function () {
  //   wx.navigateTo({
  //     url: '/pages/mine/icashbaidu/icashbaidu',
  //   })
  // }
});