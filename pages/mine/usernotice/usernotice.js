import WXPage from "../../../subwe/page";
const app = getApp();
const util = require("../../../utils/util");
WXPage({
  data: {
    COMPANY_NAME: app.globalData.COMPANY_NAME,
    COMPANY_FULL_NAME: app.globalData.COMPANY_FULL_NAME,
    phone: '',
    devInfo: {},
    showAd: 1
  },
  onLoad: async function (options) {
    if (app.globalData.deviceSn) {
      // 同步获取设备信息
      await API.getDeviceInfo({
        "deviceSn": app.globalData.deviceSn
      }).then(res => {
        let phone = res.result.prodType !== 1 ? util.ZM_PHONE_NUMBER : util.PHONE_NUMBER;
        this.setData({
          devInfo: res.result,
          showAd: res.result.isAd,
          phone: phone
        });
      });
    } else {
      this.setData({
        phone: util.PHONE_NUMBER
      });
    }
  }
});