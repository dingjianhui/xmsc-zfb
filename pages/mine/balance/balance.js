import WXPage from "../../../subwe/page";
import wx from '../../../subwe/bridge';
const app = getApp()
const util = require("../../../utils/util");
import { Index } from '../../../apis/index.js';
const API = new Index();

WXPage({
  data: {
    loginInfo: {},
    deviceSn: '',
    showAd: 1,
    memberInfo: {},
    balance: 0.00
  },
  onLoad: async function (options) {
    this.setData({
      deviceSn: app.globalData.deviceSn,
      showAd: wx.getStorageSync("showAd"),
      loginInfo: wx.getStorageSync('login_key')
    })
    if (!this.data.loginInfo) {
      await API.getLoginToken().then(res => {
        this.setData({ loginInfo: res.result })
        wx.setStorageSync("login_key",  res.result)
      })
    }

    // 获取会员信息
    var param = { "appId": my.getAppIdSync().appId, "openid": this.data.loginInfo.openid }
    await API.GetMemberInfoByOpenid(param).then(res => {
      this.setData({ memberInfo: res.result, balance: (res.result.balance/100).toFixed(2) })
    })
  },
  withdrawal() {
    let param = {}
    param.openid = this.data.loginInfo.openid
    param.appId = my.getAppIdSync().appId
    param.amount = this.data.memberInfo.balance
    console.log('提现参数', param)
    API.WithdrawalSubmit(param).then(res => {
      wx.navigateTo({url: '/pages/mine/balance/balance'})
    })
  }
});
