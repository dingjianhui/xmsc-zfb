import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import { Index } from '../../apis/index';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    StatusBar: app.globalData.StatusBar,
    tabList: [{
      name: "全部订单",
      status: 0
    }, {
      name: "待支付",
      status: 1
    }, {
      name: "待发货",
      status: 2
    }, {
      name: "订单完成",
      status: 5
    }],
    activeTab: 0,
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    orderList: [],
    pageNo: 1,
    pageSize: 20,
    totalCount: 0,
    statusList: [{
      title: "待支付",
      id: 1,
      color: "red"
    }, {
      title: "待发货",
      id: 2,
      color: "yellow"
    }, {
      title: "未知",
      id: 3,
      color: "gray"
    }, {
      title: "未知",
      id: 4,
      color: "gray"
    }, {
      title: "订单完成",
      id: 5,
      color: "green"
    }, {
      title: "已撤销",
      id: 6,
      color: "gray"
    }],
    moreVisible: false
    //订单状态 1-待支付 2-待发货 3-发货中 4-已发货 5-取消订单 6-已撤销
  },

  onLoad(options) {
    this.getGoodsOrderInfo();
  },
  // tab切换
  tabChange(e) {
    this.setData({
      activeTab: e.currentTarget.dataset.status,
      pageNo: 1,
      orderList: []
    });
    this.getGoodsOrderInfo();
  },
  // 获取所有订单列表
  getGoodsOrderInfo() {
    const nowDate = new Date().getTime();
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    const {
      pageNo,
      pageSize,
      activeTab,
      orderList
    } = this.data;
    let params = {
      pageNo,
      pageSize
    };
    if (activeTab !== 0) params.status = activeTab;
    API.getAutomateOrderList(params).then(res => {
      wx.hideLoading();
      const {
        data,
        pageNo,
        pageSize,
        totalCount
      } = res.result;
      const list = orderList.concat(data);
      list.forEach(i => {
        const createTime = new Date(i.createTime).getTime();
        const timeDifference = parseInt((nowDate - createTime) / 1000 / 60);
        i.timeDifference = true;
        if (i.status === 1 && timeDifference > 5) i.timeDifference = false;
      });
      this.setData({
        orderList: list,
        pageNo,
        pageSize,
        totalCount,
        moreVisible: list.length < totalCount
      });
    }, () => {
      wx.hideLoading();
    });
  },
  // 取消订单
  cancelOrder(e) {
    const item = e.currentTarget.dataset.item;
    let _this = this;
    wx.showModal({
      title: '提示',
      content: '确定要取消这条订单吗？',
      success(res) {
        if (res.confirm) {
          API.cancelGoodsOrder({
            tradeNo: item.tradeNo
          }).then(() => {
            wx.showToast({
              title: '订单已取消',
              icon: 'none',
              duration: 2000
            });
            _this.resetList();
          });
        }
      }
    });
  },
  resetList() {
    this.setData({
      pageNo: 1,
      orderList: []
    });
    this.getGoodsOrderInfo();
  },
  // 重新支付
  repaymentsClick(e) {
    const item = e.currentTarget.dataset.item;
    console.log(item);
    API.againGoodsOrder({
      tradeNo: item.tradeNo
    }).then(res => {
      this.wxPayment(res.result.pay);
    });
  },
  // 微信支付
  wxPayment(data) {
    // const param = JSON.parse(data);
    // let _this = this;
    // wx.requestPayment({
    //   timeStamp: param.timeStamp,
    //   nonceStr: param.nonceStr,
    //   package: param.package,
    //   signType: param.signType,
    //   paySign: param.paySign,
    //   success() {
    //     _this.resetList();
    //     wx.reLaunch({
    //       url: `/pages/supermarket/submitSuccess/submitSuccess?tradeNo=${param.tradeNo}`
    //     });
    //   },
    //   fail(res) {
    //     console.log("fail info", res);
    //   }
    // });
  },
  loadMore() {
    const {
      totalCount,
      pageNo,
      orderList
    } = this.data;
    if (orderList.length < totalCount) {
      this.setData({
        pageNo: pageNo + 1
      });
      this.getGoodsOrderInfo();
    }
  },
  workOrderNav(e) {
    const tradeNo = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/supermarket/workOrder/workOrder?tradeNo=${tradeNo}`
    });
  }
});
