import wx from '../../subwe/bridge';
import WXPage from "../../subwe/page";
import util from "../../utils/util";
const app = getApp();
import { Index } from '../../apis/index';
const API = new Index();
WXPage({
  data: {
    StatusMap: {
      1: '待处理',
      2: '待处理',
      3: '待处理',
      4: '待处理',
      5: '处理中',
      6: '已处理'
    },
    TypeMap: {
      1: '清扫服务',
      2: '日用品更换服务',
      3: '维修服务',
      4: '处理呼叫服务',
      5: '开票服务',
      6: '续住服务',
      7: '退房通知',
      9: '好评有礼服务'
    },
    evaluateMap: {
      1: '等待审核',
      2: '审核通过',
      3: '审核失败'
    },
    grantStatusMap: {
      0: '红包下发中。。。',
      1: '红包已领取，查看详情',
      2: '立即领取红包',
      3: '红包已过期'
    },
    pageType: '预约开票工单',
    dataInfo: {},
    commentInfo: {},
    imgUrl: '',
    imageUrl: `${app.globalData.imageUrl}/zhongman2`,
    defaultImg: '/static/zhongman2/img_Coupon@2x.png',
    liveDetail: {},
    host: wx.getStorageSync('host')
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      imgUrl: util.IMG_URL
    });
    let {
      id,
      commentId,
      notificationType
    } = options;
    // let {id, commentId} = {id: 80, commentId: 1}
    this.GetWorkDetails(id);
    if (commentId > 0) this.GetCommentDetails(commentId);
  },
  // 领取红包
  commentRedPacketReceive() {
    if ([1, 2].indexOf(this.data.commentInfo.grantStatus) === -1) return;
    API.commentRedPacketReceive({
      notificationId: this.data.dataInfo.id
    }).then(res => {
      if (!res.result) return wx.showToast({
        title: '领取失败，请联系管理员',
        icon: 'none'
      });
      let result = JSON.parse(res.result);
      wx.sendBizRedPacket({
        "timeStamp": result.timeStamp,
        "nonceStr": result.nonceStr,
        "package": result.package,
        "signType": result.signType,
        "paySign": result.paySign,
        "success": res => {
          this.data.commentInfo.grantStatus = 1;
          this.setData({
            commentInfo: this.data.commentInfo
          });
          API.commentRedPacketReceiveInform({
            id: Number(result.id)
          }).then(res => {
            console.log(res);
          });
        },
        "fail": res => {
          console.log(res);
          return wx.showToast({
            title: this.data.commentInfo.grantStatus === 2 ? '领取失败，请扫码进入' : '查看失败，请扫码进入',
            icon: 'none'
          });
        }
      });
    });
  },
  previewImage(ev) {
    ev.mark = ev.target.dataset
    wx.previewImage({
      urls: ev.mark.urls
    });
  },
  GetCommentDetails(id) {
    API.commentDetails({
      id: Number(id)
    }).then(res => {
      try {
        res.result.commentImgList = JSON.parse(res.result.commentImgList);
      } catch (e) {
        res.result.commentImgList = [];
      }
      this.setData({
        commentInfo: res.result
      });
    });
  },
  suggestionFeedback() {
    wx.navigateTo({
      url: '/pages/home/suggestions/suggestions'
    });
  },
  GetWorkDetails(id) {
    API.getWorkDetails({
      id: Number(id)
    }).then(res => {
      const item = res.result || {};
      if (item.notificationType === 5) {
        try {
          const {
            money,
            phone,
            taxCode,
            unitName
          } = JSON.parse(item.info);
          item.money = money;
          item.phone = phone;
          item.taxCode = taxCode;
          item.unitName = unitName;
        } catch (err) {
          item.money = '';
          item.phone = '';
          item.taxCode = '';
          item.unitName = '';
        }
      } else if (item.notificationType === 6) {
        API.continuationDetails({
          id: item.id
        }).then(rs => {
          this.setData({
            liveDetail: rs.result || {}
          });
        });
        try {
          const {
            order,
            stayOverTime,
            stayOverType
          } = JSON.parse(item.info);
          item.order = order;
          item.stayOverTime = stayOverTime;
          item.stayOverType = stayOverType;
        } catch (err) {
          item.order = '';
          item.stayOverTime = '';
          item.stayOverType = '';
        }
      } else if ([2, 3].indexOf(item.notificationType) > -1) {
        item.infoList = item.info.split('、');
      } else if (item.notificationType === 7) {
        const {
          phone
        } = JSON.parse(item.info);
        item.phone = phone;
      }
      this.setData({
        dataInfo: item
      });
    });
  }
});
