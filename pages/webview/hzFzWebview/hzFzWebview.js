import wx from '../../../subwe/bridge';
import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index.js';
const app = getApp();
const API = new Index();
WXPage({
  data: {
    showAd: 1,
    devInfo: {}
  },
  onLoad: async function (options) {
    var that = this;
    that.setData({
      showAd: wx.getStorageSync("showAd")
    });
    if (app.globalData.deviceSn) {
      // 同步获取设备信息
      await API.getDeviceInfo({
        "deviceSn": app.globalData.deviceSn
      }).then(res => {
        that.setData({
          devInfo: res.result,
          showAd: res.result.isAd
        });
      });
    }
  },
  toGetCoupon() {
    let param = {};
    API.getCoupon(param).then(res => {
      if (res.code !== 100200) {
        wx.showModal({
          title: '领取通知',
          content: res.message,
          success: res => {}
        });
      } else {
        wx.showModal({
          title: '领取通知',
          content: '领取成功',
          success: res => {}
        });
      }
    });
  }
});