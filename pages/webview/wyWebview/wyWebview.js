import WXPage from "../../../subwe/page";
import { Index } from '../../../apis/index.js';
const app = getApp();
const API = new Index();
WXPage({
  /**
   * 页面的初始数据
   */
  data: {
    COMPANY_NAME: app.globalData.COMPANY_NAME,
    COMPANY_FULL_NAME: app.globalData.COMPANY_FULL_NAME
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let deviceSn = app.globalData.deviceSn;
    let param = {
      deviceSn: deviceSn,
      appId: "wx83aeeb3f73831de9",
      logType: "wy"
    };
    API.addWyLog(param).then(res => {
      console.log("手机号码更新成功");
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {}
});