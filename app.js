import wx from './subwe/bridge';
my.global = {}
const {
  getQueryString,
  TITLE_NAME,
  COMPANY_NAME,
  COMPANY_FULL_NAME
} = require("./utils/util");
import uma from './utils/umtrack-wx/lib/index';
import { getPageInfo } from "./utils/common";
const {
  baseUrl,
  yundianUrl
} = require("./config");
App({
  startTime: 0,
  lastTime: 0,
  channel: "",
  vuid: "",
  place: "",
  // onLaunch: function () {
  //   // 第一次使用 部分配置图片不能正常展示的问题
  //   const host = wx.getStorageSync('host');
  //   if (!host) wx.setStorageSync('host', host || 'https://img.cnman.cn');
  //   this.startTime = Date.parse(new Date());
  //   // 获取设备信息
  //   wx.getSystemInfo({
  //     success: e => {
  //       this.globalData.systemInfo = e;
  //       this.globalData.TITLE_NAME = TITLE_NAME;
  //       this.globalData.COMPANY_NAME = COMPANY_NAME;
  //       this.globalData.COMPANY_FULL_NAME = COMPANY_FULL_NAME;
  //       this.globalData.windowW = e.windowWidth;
  //       this.globalData.windowH = e.windowHeight;
  //       this.globalData.StatusBar = e.statusBarHeight;
  //       let capsule = wx.getMenuButtonBoundingClientRect();
  //       if (capsule) {
  //         this.globalData.Custom = capsule;
  //         this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
  //       } else {
  //         this.globalData.CustomBar = e.statusBarHeight + 50;
  //       }
  //     }
  //   });
  // },
  onLaunch: async function (options) {

    if(options.query && !options.query.q) options.query.q = options.query.qrCode

    // 第一次使用 部分配置图片不能正常展示的问题
    const host = wx.getStorageSync('host');
    if (!host) wx.setStorageSync('host', host || 'https://api.hnxmkj.net/static');
    console.log(`wx.setStorageSync('host'`, wx.getStorageSync('host'))
    this.startTime = Date.parse(new Date());
    // 获取设备信息
    wx.getSystemInfo({
      success: e => {
        this.globalData.systemInfo = e;
        this.globalData.TITLE_NAME = TITLE_NAME;
        this.globalData.COMPANY_NAME = COMPANY_NAME;
        this.globalData.COMPANY_FULL_NAME = COMPANY_FULL_NAME;
        this.globalData.windowW = e.windowWidth;
        this.globalData.windowH = e.windowHeight;
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    });

    console.log('start', options)
    options = { query: {}, referrerInfo: {}, ...options }
    if (options.path === 'pages/padline/padline') this.globalData.pageServiceHomeOff = false;
    let scene = decodeURIComponent(options.scene);
    console.log('参数', options);
    // 携程测试
    // this.globalData.deviceSn = options.query.q
    // this.globalData.code = options.query.code
    // this.globalData.isCtrip = true
    // this.globalData.apiUrl = baseUrl
    let q = decodeURIComponent(options.query.q);
    let dev = getQueryString(q, "dev");
    let pcl = getQueryString(q, "pcl");
    if (options.query.code) {
      // 携程
      this.globalData.deviceSn = options.query.q;
      this.globalData.code = options.query.code;
      this.globalData.isCtrip = true;
      this.globalData.apiUrl = baseUrl ? baseUrl : 'https://api.hnxmkj.net/zfb/v1/wx/';
    } else if (options.query.q && options.query.t === 'super8') {
      // 速8
      this.globalData.deviceSn = options.query.q;
      this.globalData.isSuper = true;
      this.globalData.isBlue = true;
      this.globalData.apiUrl = baseUrl ? baseUrl : 'https://api.hnxmkj.net/zfb/v1/wx/';
    } else {
      if (q.includes('https')) {
        console.log(q.includes('yundian'), pcl);
        this.globalData.deviceSn = dev;
        if (q.includes('yundian') || pcl === '10041') {
          // 昀电
          this.globalData.apiUrl = yundianUrl ? yundianUrl : 'https://api.hnxmkj.net/zfb/v1/wx/';
          this.globalData.isZm = false;
        } else if (q.includes('blue.chinaman.ink')) {
          // 放水机|面板机|桌面机
          this.globalData.apiUrl = baseUrl ? baseUrl : 'https://api.hnxmkj.net/zfb/v1/wx/';
          this.globalData.isBlue = true;
        } else {
          this.globalData.apiUrl = baseUrl ? baseUrl : 'https://api.hnxmkj.net/zfb/v1/wx/';
        }
      }
    }
    // this.globalData.apiUrl = 'http://192.168.1.17:8999/v1/wx/'
    // this.globalData.apiUrl = 'https://zltest.cnman.cn/wxtoalipayapi/v1/wx/'
    this.globalData.apiUrl = 'https://api.hnxmkj.net/zfb/v1/wx/'
    const {
      deviceSn,
      apiUrl
    } = this.globalData;
    let params = {
      deviceSn: deviceSn,
      pagePath: options.path,
      senceId: options.scene.toString()
    };
    wx.login({
      success(res) {
        if (res.code) params.code = res.code;
        getPageInfo(apiUrl, params);
      }
    });
    console.log('小程序跳转进入--apiUrl, isCtrip, deviceSn, code', this.globalData.apiUrl, this.globalData.isCtrip, this.globalData.deviceSn, this.globalData.code);
  },
  updateApiUrl(q, url) {
    if (q) {
      this.globalData.apiUrl = url;
    } else {
      if (!this.globalData.apiUrl) this.globalData.apiUrl = url;
    }
  },
  updateDev(deviceSn) {
    if (!this.globalData.deviceSn) this.globalData.deviceSn = deviceSn;
  },
  updateCode(code) {
    if (!this.globalData.code) this.globalData.code = code;
  },
  updateIsCtripe(isCtrip) {
    if (!this.globalData.isCtrip) this.globalData.isCtrip = isCtrip;
  },
  setBluetoothMac(bluetoothMac) {
    this.globalData.bluetoothMac = bluetoothMac;
  },
  setBluetoothType(val) {
    console.log("bluetoothType", this.globalData.apiUrl, val);
    let bluetoothType = 1;
    if (val.pwdDecodeType === 'ZM_BD') bluetoothType = 2
    if (val.pwdDecodeType === 'ZM_PD') bluetoothType = 3
    if (val.pwdDecodeType === 'ZM_RD') bluetoothType = 4
    if (val.pwdDecodeType === 'XM_PD') bluetoothType = 5

    wx.setStorageSync("bluetoothName", val.bluetoothName || '')
    wx.setStorageSync("bluetoothType", bluetoothType);
    this.globalData.bluetoothType = bluetoothType;
  },
  toPageServiceHome(off) {
    if (this.globalData.pageServiceHomeOff) return true;
    this.globalData.pageServiceHomeOff = true;
    if (!off) wx.redirectTo({
      url: '/pages/serviceHome/serviceHome'
    });
  },
  globalData: {
    uma,
    apiUrl: "https://api.hnxmkj.net/zfb/v1/wx/",
    userInfo: {},
    deviceSn: "",
    productId: "",
    orderInfo: null,
    deviceInfo: null,
    loginInfo: {},
    isLogging: false,
    isCtrip: false,
    isZm: true,
    tradeNo: '',
    isPayScore: true,
    // 是否支付分支付
    code: "",
    //携程code
    bluetoothMac: "",
    // 蓝牙硬件地址
    bluetoothType: 1,
    // 蓝牙连接类型 1-广播模式 2-连接模式
    pageServiceHomeOff: false,
    imageUrl: "https://api.hnxmkj.net/static",
    isBlue: false,
    systemInfo: {},
    isSuper: false
  }
});
